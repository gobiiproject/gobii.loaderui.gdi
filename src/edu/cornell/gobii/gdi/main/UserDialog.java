package edu.cornell.gobii.gdi.main;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Text;
import org.gobiiproject.gobiiapimodel.payload.PayloadEnvelope;
import org.gobiiproject.gobiiapimodel.restresources.common.RestUri;
import org.gobiiproject.gobiiclient.core.gobii.GobiiClientContext;
import org.gobiiproject.gobiiclient.core.gobii.GobiiEnvelopeRestResource;
import org.gobiiproject.gobiimodel.dto.auditable.*;
import org.gobiiproject.gobiimodel.utils.LineUtils;

import edu.cornell.gobii.gdi.services.Controller;
import edu.cornell.gobii.gdi.utils.FormUtils;
import edu.cornell.gobii.gdi.utils.Utils;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;

public class UserDialog extends Dialog {
	private static Logger log = Logger.getLogger(UserDialog.class.getName());
	protected int result;
	protected Shell shlUserLogin;
	private Button btnCheckSSH;
	private Text txtService;
	private Text text;
	private Text textUsername;
	private Text textPassword;
	private Button btnGetCrops;
	private Button btnConnect;
	private Button btnOK;
	protected String currentCropName;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public UserDialog(Shell parent) {
		super(parent);
		setText("New User Information");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public int open() {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		createContents();
		shlUserLogin.open();
		shlUserLogin.layout();
		Display display = getParent().getDisplay();
		while (!shlUserLogin.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
	    
		shlUserLogin = new Shell(getParent(), SWT.BORDER | SWT.MIN | SWT.MAX | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
		shlUserLogin.setMinimumSize(new Point(400, 250));
		shlUserLogin.setSize(700, 400);
		shlUserLogin.setText("User Login");
		shlUserLogin.setLayout(new GridLayout(1, false));
		Rectangle screenSize = Display.getCurrent().getPrimaryMonitor().getBounds();
		shlUserLogin.setLocation((screenSize.width - shlUserLogin.getBounds().width) / 2, (screenSize.height - shlUserLogin.getBounds().height) / 2);

		Composite composite_2 = new Composite(shlUserLogin, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite_2.setLayout(new GridLayout(3, false));

		Label lblWebService = new Label(composite_2, SWT.RIGHT);
		lblWebService.setSize(66, 15);
		lblWebService.setText("Web Service URL:");

		Composite composite_1 = new Composite(composite_2, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		composite_1.setSize(353, 34);
		composite_1.setLayout(new GridLayout(2, false));

		txtService = new Text(composite_1, SWT.BORDER);
		txtService.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				App.INSTANCE.setService(txtService.getText());
				if(txtService.getText().contains("localhost:")) btnCheckSSH.setSelection(false);
				btnCheckSSH.setEnabled(true);

			}
		});

		txtService.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		new Label(composite_2, SWT.NONE);

		btnCheckSSH = new Button(composite_2, SWT.CHECK);
		btnCheckSSH.setSize(42, 16);
		btnCheckSSH.setSelection(true);
		btnCheckSSH.setText("SSH");
		new Label(composite_2, SWT.NONE);

		Label label = new Label(composite_2, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));


		Label lblUser = new Label(composite_2, SWT.NONE);
		lblUser.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblUser.setText("Username:");

		textUsername = new Text(composite_2, SWT.BORDER);
		textUsername.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		Label lblPassword = new Label(composite_2, SWT.NONE);
		GridData gd_lblNewLabel = new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1);
		gd_lblNewLabel.horizontalIndent = 1;
		lblPassword.setLayoutData(gd_lblNewLabel);
		lblPassword.setText("Password:");

		textPassword = new Text(composite_2,  SWT.BORDER| SWT.PASSWORD);
		textPassword.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		textPassword.addListener(SWT.Traverse, new Listener() {
			@Override
			public void handleEvent(Event event)
			{
				if(event.detail == SWT.TRAVERSE_RETURN)
				{
					if(textUsername.getText().isEmpty() || textPassword.getText().isEmpty()){

						Utils.showErrorInfo(log, "Please enter a username and password");
						return;
					}
					connect(false); 
				}
			}
		});

		Label label_1 = new Label(composite_2, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		new Label(composite_2, SWT.NONE);

		Composite composite = new Composite(composite_2, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		composite.setSize(353, 49);
		composite.setLayout(new GridLayout(2, false));

		btnOK = new Button(composite, SWT.NONE);
		btnOK.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(textUsername.getText().isEmpty() || textPassword.getText().isEmpty() || App.INSTANCE.getService().isEmpty() || App.INSTANCE.getService() == null ){

					Utils.showLog(log, "All fields are required", new Throwable("A field is empty"));
				}
				else {
					if(connect(true)){
						App.INSTANCE.save();
						result = Window.OK;
						shlUserLogin.close();
					}
				} 
			}
		});
		btnOK.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnOK.setText("Connect");

		Button btnCancel = new Button(composite, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = Window.CANCEL;
				shlUserLogin.close();
			}
		});
		btnCancel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnCancel.setText("Cancel");
		shlUserLogin.setDefaultButton(btnOK);

		createContent();

	}

	protected boolean connect(boolean displayErrors) {
		// TODO Auto-generated method stub
		boolean isLoginSuccessful = false;

		// try to connect to service 

		if(!App.INSTANCE.getService().endsWith("/")){
			String service = App.INSTANCE.getService() +"/";
			txtService.setText(service);
			App.INSTANCE.setService(service);
		}
		Boolean isCropRetrieved =  Controller.getCrop(log, true, btnCheckSSH.getSelection(),true);

		//check if there is a crop to the server
		if(isCropRetrieved){
			btnCheckSSH.setEnabled(false);

			currentCropName = App.INSTANCE.getCrop();

			//login using credentials entered by the user
			if(Controller.authenticate(currentCropName, textUsername.getText(),textPassword.getText())){

				PayloadEnvelope<ContactDTO> resultEnvelope;

				try {
					resultEnvelope = Controller.getContactByUsername(textUsername.getText());

					if(Controller.getDTOResponse(Display.getCurrent().getActiveShell(), resultEnvelope.getHeader(), null, false)){

						ContactDTO contactDTO = resultEnvelope.getPayload().getData().get(0);

						//check first if curator
						List<Integer> userRoles = contactDTO.getRoles();

						boolean isCurator=false;
						boolean isAdmin=false; //Adding Admin role for GP1-2250
							
						Integer curatorRoleId = Controller.getCuratorRoleId();
						Integer adminRoleId = Controller.getAdminRoleId();
						
						
						for(Integer i: userRoles){

							if(i == curatorRoleId){

								isCurator = true;

							}
							else if(i == adminRoleId){

								isAdmin = true;

							}
						}

						if(isCurator || isAdmin ){

							App.INSTANCE.setCrop(currentCropName);
							App.INSTANCE.getUser().setUserEmail(contactDTO.getEmail());
							App.INSTANCE.getUser().setUserName(contactDTO.getUserName());
							App.INSTANCE.getUser().setUserId(contactDTO.getContactId());
							App.INSTANCE.getUser().setUserFullname(contactDTO.getLastName() +", "+ contactDTO.getFirstName());
							
							isLoginSuccessful = true;
							
							currentCropName = FormUtils.getCurrentCropType();

						}
						else{

							MessageDialog.openError(shlUserLogin, "Error: Not a Curator or Admin.", "The user does not have the Curator or the Admin role.");
							isLoginSuccessful = false;
						}

					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block

					Utils.showLog(log, "Error authenticating. ", e1);
				}
			}
		}

		return isLoginSuccessful;
	}


	protected void createContent(){
		if(App.INSTANCE.getService() != null) txtService.setText(App.INSTANCE.getService());

	}

}
