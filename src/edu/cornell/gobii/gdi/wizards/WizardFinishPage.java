package edu.cornell.gobii.gdi.wizards;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import edu.cornell.gobii.gdi.objects.xml.Columns;
import edu.cornell.gobii.gdi.utils.Utils;
import edu.cornell.gobii.gdi.utils.WizardUtils;
import edu.cornell.gobii.gdi.wizards.datasets.DTOdataset;
import edu.cornell.gobii.gdi.wizards.dnasamples.DTOsamples;
import edu.cornell.gobii.gdi.wizards.dnasamples.Pg1DNAsamples;
import edu.cornell.gobii.gdi.wizards.dnasamples.WizardDNAsamples;
import edu.cornell.gobii.gdi.wizards.markers.DTOmarkers;

import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.wb.swt.SWTResourceManager;
import org.gobiiproject.gobiimodel.cvnames.JobPayloadType;
import org.gobiiproject.gobiimodel.dto.instructions.loader.LoaderInstructionFilesDTO;
import org.osgi.dto.DTO;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.eclipse.swt.custom.CLabel;

public class WizardFinishPage extends WizardPage {
	private static Logger log = Logger.getLogger(WizardFinishPage.class.getName());

	private String config;
	private String wizardType;
	private DTOsamples dtoSamples;
	private DTOmarkers dtoMarkers;
	private DTOdataset dtoDataset;
	private Label lblPI;
	private Label labelProject;
	private Label labelExperiment;
	private Label lblDS;
	private Label lblDSType;
	private Label lblPlatform;
	private Label lblMapset;
	private Label piName;
	private Label projectName;
	private Label experimentName;
	private Label datasetName;
	private Label dsTypeName;
	private Label platformName;
	private Label mapsetName;
	private WizardPage firstPage;

	private boolean initialized = false;
	private StyledText styledText;
	private TabFolder tabFolder;
	private TabItem tbtmSummary;
	private TabItem tbtmFilePreview;
	private Label label;
	private Label label_1;
	private Label label_2;
	private Label label_3;
	private Label label_4;
	private Label label_5;
	private Label label_6;
	private Label label_7;
	/**
	 * Create the wizard.
	 * @param firstPage 
	 */
	public WizardFinishPage(String config, String title, String wizardType, DTOsamples sampleDTO, DTOmarkers markerDTO, DTOdataset datasetDTO, WizardPage firstPage) {
		super("wizardPage");
		setTitle(title);
		setDescription("");
		this.config = config;
		dtoDataset = datasetDTO;
		dtoMarkers = markerDTO;
		dtoSamples = sampleDTO;
		this.wizardType = wizardType;
		this.firstPage = firstPage;
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);

		setControl(container);
		container.setLayout(new GridLayout(1, false));

		tabFolder = new TabFolder(container, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		tbtmSummary = new TabItem(tabFolder, SWT.NONE);
		tbtmSummary.setText("Summary");

		Composite composite = new Composite(tabFolder, SWT.NONE);
		tbtmSummary.setControl(composite);
		composite.setLayout(new GridLayout(3, false));

		label = new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		lblPI = new Label(composite, SWT.NONE);
		lblPI.setText("PI:");

		label_1 = new Label(composite, SWT.NONE);

		piName = new Label(composite, SWT.NONE);
		piName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		piName.setText("<empty>");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		label_2 = new Label(composite, SWT.NONE);

		labelProject = new Label(composite, SWT.NONE);
		labelProject.setText("Project:");
		new Label(composite, SWT.NONE);

		projectName = new Label(composite, SWT.NONE);
		projectName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		projectName.setText("<empty>");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		label_3 = new Label(composite, SWT.NONE);

		labelExperiment = new Label(composite, SWT.NONE);
		labelExperiment.setText("Experiment:");
		new Label(composite, SWT.NONE);

		experimentName = new Label(composite, SWT.NONE);
		experimentName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		experimentName.setText("<empty>");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		label_4 = new Label(composite, SWT.NONE);

		lblDS = new Label(composite, SWT.NONE);
		lblDS.setText("Dataset:");
		new Label(composite, SWT.NONE);

		datasetName = new Label(composite, SWT.NONE);
		datasetName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		datasetName.setText("<empty>");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		label_5 = new Label(composite, SWT.NONE);

		lblDSType = new Label(composite, SWT.NONE);
		lblDSType.setText("Dataset Type:");
		new Label(composite, SWT.NONE);

		dsTypeName = new Label(composite, SWT.NONE);
		dsTypeName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		dsTypeName.setText("<empty>");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		label_6 = new Label(composite, SWT.NONE);

		lblPlatform = new Label(composite, SWT.NONE);
		lblPlatform.setText("Platform:");
		new Label(composite, SWT.NONE);

		platformName = new Label(composite, SWT.NONE);
		platformName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		platformName.setText("<empty>");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		label_7 = new Label(composite, SWT.NONE);

		lblMapset = new Label(composite, SWT.NONE);
		lblMapset.setText("Mapset");
		new Label(composite, SWT.NONE);

		mapsetName = new Label(composite, SWT.NONE);
		mapsetName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		mapsetName.setText("<empty>");


		//disable all except Proj and exp.
		lblDS.setEnabled(wizardType.equals("Dataset"));
		datasetName.setEnabled(wizardType.equals("Dataset"));

		lblDSType.setEnabled(wizardType.equals("Dataset"));
		dsTypeName.setEnabled(wizardType.equals("Dataset"));

		lblPlatform.setEnabled(!wizardType.equals("Sample"));
		platformName.setEnabled(!wizardType.equals("Sample"));

		lblMapset.setEnabled(!wizardType.equals("Sample"));
		mapsetName.setEnabled(!wizardType.equals("Sample"));

		tbtmFilePreview = new TabItem(tabFolder, SWT.NONE);
		tbtmFilePreview.setText("File Preview");

		styledText = new StyledText(tabFolder, SWT.V_SCROLL | SWT.WRAP | SWT.BORDER);
		tbtmFilePreview.setControl(styledText);
		styledText.setEditable(false);
		styledText.setDoubleClickEnabled(false);


		container.addPaintListener(new PaintListener()
		{
			@Override
			public void paintControl(PaintEvent e)
			{
				if (!initialized ) {
					initialized = true;
					validateAccordingToType();
				}	
				//				container.redraw();


			}
		});


	}

	private void validateAccordingToType() {
		// TODO Auto-generated method stub
		boolean validated = true;
		String jsonFromInstructions;
		LoaderInstructionFilesDTO instructions = new LoaderInstructionFilesDTO();

		if(wizardType.equals("Sample")){ 
			//check for required values

			if (dtoSamples.getSampleFields().size() > 0 && dtoSamples.getProjectID() == null) {
				Utils.showLog(log, "Project is required for DNA Sample information",
						new Exception("No Project specified!"));
				validated = false;
			} else if (dtoSamples.getRunFields().size() > 0 && dtoSamples.getExperimentID() == null) {
				Utils.showLog( log, "Experiment is required forDNA Run information",
						new Exception("No Experiment specified!"));
				validated = false;
			} else if (!areSampleFieldsMapped(dtoSamples)){
				Utils.showLog( log, "You have not mapped any fields yet.",
						new Exception("The fields have not been mapped."));
				validated = false;
			}
			else { //still display avail information

				try{	

					//populate other values
					setPiName(dtoSamples.getPiName());
					setProjectName(dtoSamples.getProjectName());
					setExperimentName(dtoSamples.getExperimentName());
				}
				catch(NullPointerException ioe){

					validated = false;
				}

				if(validated){ // create instruction file
					String folder = new File(dtoSamples.getPreviewDTO().getDirectoryName()).getName();
					WizardUtils.createSampleInstructionsFromDTO(getShell(), instructions, dtoSamples, folder, false);
					instructions.setInstructionFileName(folder);
					try{
						instructions.getProcedure().getMetadata().setJobPayloadType(JobPayloadType.CV_PAYLOADTYPE_SAMPLES);
					}catch(IndexOutOfBoundsException ioe){
						validated = false;
						MessageDialog.openError(Display.getDefault().getActiveShell(), "Error in LOADER", "Cannot create preview.\n\nPlease fill out required fields");
					}
				}
			}

		} else if(wizardType.equals("Marker")){
			if(dtoMarkers.getMarkerFields().size() > 0 && dtoMarkers.getPlatformID() == null){
				Utils.showLog( log, "Platform is required for Marker information", new Exception("No Platform specified!"));
				validated = false;
			} else if(dtoMarkers.getDsMarkerFields().size() > 0 && dtoMarkers.getDatasetID() == null){
				Utils.showLog( log, "Dataset is required for DS Marker information", new Exception("No Dataset specified!"));
				validated = false;
			} else if(dtoMarkers.getLgFields().size() > 0 && dtoMarkers.getMapsetID() == null){
				Utils.showLog(log, "Mapset is required for LG information", new Exception("No Mapset specified!"));
				validated = false;
			} else if(dtoMarkers.getLgMarkerFields().size() > 0 && dtoMarkers.getPlatformID()==null){
				Utils.showLog(log, "Platform is required for Marker_LG information", new Exception("No Mapset specified!"));
				validated = false;
			}else if (!areMarkersFieldsMapped(dtoMarkers)){
				Utils.showLog( log, "You have not mapped any fields yet.",
						new Exception("The fields have not been mapped."));
				validated = false;
			}
			else{ //still display avail information

				try{	

					setPiName(dtoMarkers.getPiName());
					setProjectName(dtoMarkers.getProjectName());
					setExperimentName(dtoMarkers.getExperimentName());
					setMapsetName(dtoMarkers.getMapsetName());
					setPlatformName(dtoMarkers.getPlatformName());
				}
				catch(NullPointerException ioe){

					validated = false;
				}

				if(validated){
					String folder = new File(dtoMarkers.getPreviewDTO().getDirectoryName()).getName();

					WizardUtils.createMarkerInstructionsFromDTO(getShell(), instructions, dtoMarkers, folder, false);

					instructions.setInstructionFileName(folder);
					try{
						instructions.getProcedure().getMetadata().setJobPayloadType(JobPayloadType.CV_PAYLOADTYPE_MARKERS);
					}catch(IndexOutOfBoundsException ioe){
						validated = false;
						MessageDialog.openError(Display.getDefault().getActiveShell(), "Error in LOADER", "Cannot create preview.\n\nPlease fill out required fields");
					}
				}
			}
		} else { // if wizard type is Dataset

			if(dtoDataset.getDatasetID() == null){
				Utils.showLog(log, "Dataset is required information", new Exception("No Dataset specified!"));
				validated = false;
			} else if(dtoDataset.getDatasetTypeID() == null){
				Utils.showLog(log, "Dataset Type is required information", new Exception("No Dataset Type specified!"));
				validated = false;
			} else if(dtoDataset.getDtoMarkers().getMarkerFields().size() > 0 && dtoDataset.getPlatformID() == null){
				Utils.showLog(log, "Platform is required for Marker information", new Exception("No Platform specified!"));
				validated = false;
			} else if(dtoDataset.getDtoMarkers().getDsMarkerFields().size() > 0 && dtoDataset.getDatasetID() == null){
				Utils.showLog(log, "Dataset is required for DS Marker information", new Exception("No Dataset specified!"));
				validated = false;
			} else if(dtoDataset.getDtoMarkers().getDsMarkerFields().size() > 0 && dtoDataset.getPlatformID() == null){
				Utils.showLog(log, "Platform is required for DS Marker information", new Exception("No Platform specified!"));
				validated = false;
			} else if(dtoDataset.getDtoMarkers().getLgFields().size() > 0 && dtoDataset.getMapsetID() == null){
				Utils.showLog(log, "Mapset is required for LG information", new Exception("No Mapset specified!"));
				validated = false;
			} else if(dtoDataset.getDtoMarkers().getLgMarkerFields().size() > 0 && dtoDataset.getPlatformID()==null){
				Utils.showLog(log, "Platform is required for Marker_LG information", new Exception("No Mapset specified!"));
				validated = false;
			} else if(dtoDataset.getDtoSamples().getSampleFields().size() > 0 && dtoDataset.getProjectID() == null){
				Utils.showLog(log, "Project is required for DNA Sample information", new Exception("No Project specified!"));
				validated = false;
			} else if(dtoDataset.getDtoSamples().getRunFields().size() > 0 && dtoDataset.getExperimentID() == null){
				Utils.showLog(log, "Experiment is required forDNA Run information", new Exception("No Experiment specified!"));
				validated = false;
			} else if (!areSampleFieldsMapped(dtoDataset.getDtoSamples()) && !areMarkersFieldsMapped(dtoDataset.getDtoMarkers()) && dtoDataset.getDtoMarkers().getDsMarkerFields().size()  <1){ //check if there is actually a field mapped in the wizard
				validated = false;
				Utils.showLog( log, "You have not mapped any fields yet.",
						new Exception("The fields have not been mapped."));
			}
			else{ //still display avail information

				try{	
					setPiName(dtoDataset.getPiName());
					setProjectName(dtoDataset.getProjectName());
					setExperimentName(dtoDataset.getExperimentName());
					setMapsetName(dtoDataset.getMapsetName());
					setPlatformName(dtoDataset.getPlatformName());
					setDatasetName(dtoDataset.getDatasetName());
					setDatasetType(dtoDataset.getDatasetType());
				}
				catch(NullPointerException ioe){

					validated = false;
				}

				if(validated){
					String folder = new File(dtoDataset.getPreviewDTO().getDirectoryName()).getName();

					WizardUtils.createDatasetInstructionsFromDTO(getShell(), dtoDataset, instructions, folder);

					instructions.setInstructionFileName(folder);
					try{
						instructions.getProcedure().getMetadata().setJobPayloadType(JobPayloadType.CV_PAYLOADTYPE_MATRIX);
					}catch(IndexOutOfBoundsException ioe){
						MessageDialog.openError(Display.getDefault().getActiveShell(), "Error in LOADER", "Cannot create preview.\n\nPlease fill out required fields");
						validated = false;
					}
				}
			}
		}

		if(validated){
			setPageComplete(true);
			firstPage.setPageComplete(true);

			//display Json
			try {
				jsonFromInstructions = WizardUtils.createJsonTextFromInstructions(instructions);
				styledText.setText(jsonFromInstructions);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				Utils.showLog(log, "Platform is required for Marker information", new Exception("Error creating intructions for display!"));
				//				e.printStackTrace();
			}

		}else{
			setPageComplete(false);
			
		}

		getContainer().updateButtons();
	}

	private boolean areMarkersFieldsMapped(DTOmarkers dtoMarkers2) {
		// TODO Auto-generated method stub
		boolean allFieldsSelected = true;
		
		if(dtoMarkers2.getMarkerFields().size() <1 && dtoMarkers2.getLgMarkerFields().size()<1 && dtoMarkers2.getLgFields().size() <1) allFieldsSelected = false;
		
		return allFieldsSelected;
	}

	private boolean areSampleFieldsMapped(DTOsamples dtoSamples2) {
		// TODO Auto-generated method stub
		boolean allFieldsSelected = true;

		if(dtoSamples2.getRunFields().size() <1 && dtoSamples2.getSampleFields().size()<1 && dtoSamples2.getGermplasmFields().size() <1) allFieldsSelected = false;

		return allFieldsSelected;
	}

	private void setDatasetType(String datasetType) {
		// TODO Auto-generated method stub
		if(datasetType != null) dsTypeName.setText(datasetType);
		else dsTypeName.setText("<empty>");
	}

	private void setDatasetName(String datasetName2) {
		// TODO Auto-generated method stub
		if(datasetName2 != null) datasetName.setText(datasetName2);
		else datasetName.setText("<empty>");
	}

	private void setPlatformName(String platformName2) {
		// TODO Auto-generated method stub

		if(platformName2 != null) platformName.setText(platformName2);
		else platformName.setText("<empty>");
	}

	private void setMapsetName(String mapsetName2) {
		// TODO Auto-generated method stub

		if(mapsetName2 != null) mapsetName.setText(mapsetName2);
		else mapsetName.setText("<empty>");
	}

	private void setExperimentName(String experimentName2) {
		// TODO Auto-generated method stub

		if(experimentName2 != null) experimentName.setText(experimentName2);
		else experimentName.setText("<empty>");
	}

	private void setProjectName(String projectName2) {
		// TODO Auto-generated method stub

		if(projectName2 != null) projectName.setText(projectName2);
		else projectName.setText("<empty>");
	}

	private void setPiName(String piName2) {
		// TODO Auto-generated method stub
		if(piName2 != null) piName.setText(piName2);
		else piName.setText("<empty>");
	}

	/** @override */
	public org.eclipse.jface.wizard.IWizardPage getPreviousPage() {
		//kind of hack to detect without overriding WizardDialog#nextPressed()
		boolean isBackPressed = "backPressed".equalsIgnoreCase(Thread.currentThread().getStackTrace()[2].getMethodName());
		if (isBackPressed) {
			setPageComplete(false);
			getContainer().updateButtons();
			initialized = false;
		}
		return super.getPreviousPage();
	}
}