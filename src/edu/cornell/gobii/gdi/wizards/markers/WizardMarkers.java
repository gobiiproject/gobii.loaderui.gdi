package edu.cornell.gobii.gdi.wizards.markers;

import java.io.File;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.gobiiproject.gobiiapimodel.payload.PayloadEnvelope;
import org.gobiiproject.gobiiapimodel.restresources.gobii.GobiiUriFactory;
import org.gobiiproject.gobiiclient.core.gobii.GobiiClientContext;
import org.gobiiproject.gobiiclient.core.gobii.GobiiEnvelopeRestResource;
import org.gobiiproject.gobiimodel.config.RestResourceId;
import org.gobiiproject.gobiimodel.cvnames.JobPayloadType;
import org.gobiiproject.gobiimodel.dto.instructions.loader.*;
import org.gobiiproject.gobiimodel.types.GobiiFileProcessDir;
import org.gobiiproject.gobiimodel.types.GobiiProcessType;
import org.gobiiproject.gobiimodel.utils.InstructionFileValidator;

import edu.cornell.gobii.gdi.main.App;
import edu.cornell.gobii.gdi.main.Main2;
import edu.cornell.gobii.gdi.services.Controller;
import edu.cornell.gobii.gdi.utils.Utils;
import edu.cornell.gobii.gdi.utils.WizardUtils;
import edu.cornell.gobii.gdi.utils.WizardUtils.TemplateCode;
import edu.cornell.gobii.gdi.wizards.WizardFinishPage;

public class WizardMarkers extends Wizard {

	private static Logger log = Logger.getLogger(WizardMarkers.class.getName());
	private String config;
	private DTOmarkers dto = new DTOmarkers();
	private int piID;
	private int projectID;
	private int experimentID;
	private int datasetID;

	public WizardMarkers(String config, int piID, int projectID, int experimentID, int datasetID){
		setWindowTitle("Marker Data Loading Wizard");
		this.config = config;

		this.piID=piID;
		this.projectID = projectID;
		this.experimentID = experimentID;
		this.datasetID = datasetID;
	}

	@Override
	public void addPages() {
		Pg1Markers firstPage = new Pg1Markers(config, dto, piID, projectID, experimentID, datasetID);
		addPage(firstPage);
		addPage(new Pg2Markers(config, dto));
		addPage(new Pg3Markers(config, dto));
		addPage(new WizardFinishPage(config, "Wizard :: Marker Information", "Marker", null, dto, null, firstPage));
	}

	@Override
	public boolean performFinish() {

		try{
			String folder = new File(dto.getPreviewDTO().getDirectoryName()).getName();
			if(folder != null || !folder.isEmpty()){
				WizardUtils.createInstructionsForWizard(folder, dto);
			}else{
				Utils.showLog( log, "Error submitting instruction file", new Exception("No source file(s) specified!"));
				return false;
			}

			LoaderInstructionFilesDTO instructions = null;
			if(dto.getTemplate() != null){
				instructions = WizardUtils.loadTemplate(dto.getTemplate());
				if(!WizardUtils.createMarkerInstructionsFromTemplate(getShell(), instructions, dto, folder)){
					return false;
				}
			}else{
				instructions = new LoaderInstructionFilesDTO();
				WizardUtils.createMarkerInstructionsFromDTO(getShell(), instructions, dto, folder, false);
			}

			instructions.setInstructionFileName(folder);
			instructions.getProcedure().getMetadata().setJobPayloadType(JobPayloadType.CV_PAYLOADTYPE_MARKERS);
			
			if(WizardUtils.confirm("Do you want to submit data?")){

				InstructionFileValidator instructionFileValidator = new InstructionFileValidator(instructions.getProcedure());

				instructionFileValidator.processInstructionFile();

				String validationStatus = instructionFileValidator.validateMarkerUpload();
				if(validationStatus != null){

					MessageDialog.openError(Display.getCurrent().getActiveShell(), "Instruction file validation failed.", validationStatus);
					return false;
					
				}else{

				try {
					PayloadEnvelope<LoaderInstructionFilesDTO> payloadEnvelope = new PayloadEnvelope<>(instructions, GobiiProcessType.CREATE);
					String currentCropContextRoot = GobiiClientContext.getInstance(null, false).getCurrentCropContextRoot();
					GobiiUriFactory gobiiUriFactory = new GobiiUriFactory(currentCropContextRoot);
					GobiiEnvelopeRestResource<LoaderInstructionFilesDTO,LoaderInstructionFilesDTO> restResource = new GobiiEnvelopeRestResource<>(gobiiUriFactory.resourceColl(RestResourceId.GOBII_FILE_LOAD_INSTRUCTIONS));
					PayloadEnvelope<LoaderInstructionFilesDTO> loaderInstructionFileDTOResponseEnvelope = restResource.post(LoaderInstructionFilesDTO.class,
							payloadEnvelope);
					if(!Controller.getDTOResponse(this.getShell(), loaderInstructionFileDTOResponseEnvelope.getHeader(), null, true)){
						return false;
					}

				} catch (Exception err) {
					Utils.showLog( log, "Error submitting instruction file", err);
					return false;
				}
			}
		}

		WizardUtils.saveTemplate(getShell(), TemplateCode.MKR, instructions);
	}catch(Exception err){
		Utils.showLog( log, "Error submitting instruction file", err);
		return false;
	}
	return true;

}

}
