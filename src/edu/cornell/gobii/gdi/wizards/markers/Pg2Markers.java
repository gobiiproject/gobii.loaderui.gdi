package edu.cornell.gobii.gdi.wizards.markers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import edu.cornell.gobii.gdi.utils.Utils;
import org.eclipse.swt.custom.SashForm;


public class Pg2Markers extends WizardPage {
	private String config;
	private DTOmarkers dto;
	private Table tbFieldHeaders;
	private Table tbMarkerProp;
	private Table tbMarker;
	private Table tbDSmarker;

	/**
	 * Create the wizard.
	 */
	public Pg2Markers(String config, DTOmarkers dto) {
		super("wizardPage");
		setTitle("Wizard :: Marker Information");
		setDescription("");
		this.config = config;
		this.dto = dto;
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
	    SashForm container = new SashForm(parent, SWT.HORIZONTAL);
	    int[] weights = {2,5,5};

		setControl(container);
		container.setLayout(new GridLayout(3, false));

		Group grpDataFileFields = new Group(container, SWT.NONE);
		GridData gd_grpInformation = new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1);
		gd_grpInformation.widthHint = 350;
		grpDataFileFields.setLayoutData(gd_grpInformation);
		grpDataFileFields.setLayout(new GridLayout(2, false));
		grpDataFileFields.setText("Data file fields");
		
		tbFieldHeaders = new Table(grpDataFileFields, SWT.BORDER | SWT.FULL_SELECTION);
		tbFieldHeaders.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tbFieldHeaders.setHeaderVisible(true);
		tbFieldHeaders.setLinesVisible(true);

		TableColumn tblclmnFieldHeaders = new TableColumn(tbFieldHeaders, SWT.NONE);
		tblclmnFieldHeaders.setWidth(200);
		tblclmnFieldHeaders.setText("Field Headers");
		Utils.setDndColumnSource(tbFieldHeaders);

		SashForm composite = new SashForm(container, SWT.VERTICAL);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		tbMarker = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData gd_tbMarker = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tbMarker.minimumHeight = 150;
		gd_tbMarker.minimumWidth = 100;
		tbMarker.setLayoutData(gd_tbMarker);
		tbMarker.setHeaderVisible(true);
		tbMarker.setLinesVisible(true);

		TableColumn tblclmnIndex = new TableColumn(tbMarker, SWT.NONE);
		tblclmnIndex.setWidth(100);

		TableColumn tblclmnMarkerInformation = new TableColumn(tbMarker, SWT.NONE);
		tblclmnMarkerInformation.setWidth(250);
		tblclmnMarkerInformation.setText("Marker Information");

		TableColumn tblclmnHeader = new TableColumn(tbMarker, SWT.NONE);
		tblclmnHeader.setWidth(200);
		tblclmnHeader.setText("Header");

		TableColumn tblclmnFrom = new TableColumn(tbMarker, SWT.NONE);
		tblclmnFrom.setWidth(100);
		tblclmnFrom.setText("From");

		TableColumn tblclmnTo = new TableColumn(tbMarker, SWT.NONE);
		tblclmnTo.setWidth(100);
		tblclmnTo.setText("To");

		Utils.unmarshalColumns(tbMarker, config+"/xml/Marker.xml", dto.getMarkerFields(), dto.getSubMarkerFields());
		Utils.setDndColumnTarget(tbFieldHeaders, tbMarker, dto.getMarkerFields(), dto.getSubMarkerFields());
		Utils.setTableMouseLister(tbMarker, dto.getMarkerFields());

		tbMarkerProp = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_tbMarkerProp = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tbMarkerProp.minimumWidth = 100;
		gd_tbMarkerProp.minimumHeight = 150;
		tbMarkerProp.setLayoutData(gd_tbMarkerProp);
		tbMarkerProp.setHeaderVisible(true);
		tbMarkerProp.setLinesVisible(true);

		TableColumn tblclmnIndex_2 = new TableColumn(tbMarkerProp, SWT.NONE);
		tblclmnIndex_2.setWidth(100);

		TableColumn tblclmnProperty = new TableColumn(tbMarkerProp, SWT.NONE);
		tblclmnProperty.setWidth(250);
		tblclmnProperty.setText("Property");

		TableColumn tblclmnValue = new TableColumn(tbMarkerProp, SWT.NONE);
		tblclmnValue.setWidth(200);
		tblclmnValue.setText("Value");

		Utils.loadTableProps(tbMarkerProp, "marker_prop", dto.getMarkerPropFields());
		Utils.setDndColumnTarget(tbFieldHeaders, tbMarkerProp, dto.getMarkerPropFields(), null);
//		Utils.setTableMouseLister(tbMarkerProp, dto.getMarkerPropFields());

		tbDSmarker = new Table(container, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_tbDSmarker = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tbDSmarker.minimumWidth = 100;
		tbDSmarker.setLayoutData(gd_tbDSmarker);
		tbDSmarker.setHeaderVisible(true);
		tbDSmarker.setLinesVisible(true);
		tbDSmarker.setEnabled(false);

		TableColumn tblclmnIndex_1 = new TableColumn(tbDSmarker, SWT.NONE);
		tblclmnIndex_1.setWidth(100);

		TableColumn tblclmnDsMarkerInformation = new TableColumn(tbDSmarker, SWT.NONE);
		tblclmnDsMarkerInformation.setWidth(250);
		tblclmnDsMarkerInformation.setText("DS Marker Information");

		TableColumn tblclmnHeader_1 = new TableColumn(tbDSmarker, SWT.NONE);
		tblclmnHeader_1.setWidth(200);
		tblclmnHeader_1.setText("Header");

		TableColumn tblclmnFrom_1 = new TableColumn(tbDSmarker, SWT.NONE);
		tblclmnFrom_1.setWidth(100);
		tblclmnFrom_1.setText("From");

		TableColumn tblclmnTo_1 = new TableColumn(tbDSmarker, SWT.NONE);
		tblclmnTo_1.setWidth(100);
		tblclmnTo_1.setText("To");

		Utils.unmarshalColumns(tbDSmarker, config+"/xml/DS_marker.xml", dto.getDsMarkerFields(), dto.getSubDsMarkerFields());
		Utils.setDndColumnTarget(tbFieldHeaders, tbDSmarker, dto.getDsMarkerFields(), dto.getSubDsMarkerFields());
		Utils.setTableMouseLister(tbDSmarker, dto.getDsMarkerFields());

		TableColumn tblclmnPreview = new TableColumn(tbFieldHeaders, SWT.NONE);
		tblclmnPreview.setWidth(250);
		tblclmnPreview.setText("Preview");
		new Label(grpDataFileFields, SWT.NONE);

        container.setWeights(weights);
		container.addListener(SWT.Show, new Listener(){

			@Override
			public void handleEvent(Event arg0) {
				tbFieldHeaders.clearAll();
				tbFieldHeaders.removeAll();
				for(int i=0; i<dto.getHeader().size(); i++){
					TableItem item = new TableItem(tbFieldHeaders, SWT.NONE);
					item.setText(dto.getHeader().get(i));
					//					item.setText(0, dto.getHeader().get(i)[0]);
				}
				tbMarker.setEnabled(dto.getPlatformID() != null);
				tbMarkerProp.setEnabled(dto.getPlatformID() != null);
				tbDSmarker.setEnabled(dto.getDatasetID() != null);
			}

		});
	}


	/** @override */
	public org.eclipse.jface.wizard.IWizardPage getNextPage() {
		//kind of hack to detect without overriding WizardDialog#nextPressed()
		boolean isNextPressed = "nextPressed".equalsIgnoreCase(Thread.currentThread().getStackTrace()[2].getMethodName());
		if (isNextPressed) {
			boolean validatedNextPress = this.nextPressed();
			if (!validatedNextPress) {
				return this;
			}
		}
		return super.getNextPage();
	}

	/**
	 * @see WizardDialog#nextPressed()
	 * @see WizardPage#getNextPage()
	 */
	protected boolean nextPressed() {
		boolean validatedNextPressed = true;
		boolean validMarker = true;
		boolean validMarkerProp = true;
		boolean validMarkerDS = true;
		String message = "";
		try {
			// validate marker table
			if(dto.getMarkerFields().size() > 0){
				
				// Required fields
				// name, 
				if(!dto.getMarkerFields().containsKey("name")){
					validMarker = false;
					message += "Invalid Name field!\n";
				}else{
					dto.setMarkerFieldName((String) tbMarker.getData("name"));
				}
				
				if(!(dto.getPlatformID()>0)){
					validMarker = false;
					message += "A platform must be selected!\n";
				}
			}
			// validate marker prop table

			if(dto.getMarkerPropFields().size() > 0){ 
				if(dto.getMarkerFields().size() > 0)
					validMarkerProp = true;
				else{
					validMarkerProp = false;
					message += "Marker table must be filled out!";
				}
			}

			if(dto.getDsMarkerFields().size() > 0 || tbDSmarker.isEnabled()){ //if not DS Wizard it'll be disabled and therefore empty
				
				// Required fields: Marker_name
				if(!dto.getDsMarkerFields().containsKey("marker_name")){
					validMarker = false;
					message += "DS Marker marker_name is required!\n";
				} else if(dto.getDsMarkerFields().containsKey("marker_name") && dto.getMarkerFields().containsKey("name")){

					if(!(dto.getMarkerFieldName()).equalsIgnoreCase((String) tbDSmarker.getData("marker_name"))){
						
						validMarkerDS = false;
						
						message += "Marker Information 'name' do not match with the 'marker_name' field under the DS Marker Information.\n";
						
					}
				}
			}
			if(!validMarker || !validMarkerProp || !validMarkerDS){
				MessageDialog.openError(Display.getCurrent().getActiveShell(), "Invalid Mapping", message);
				return false;
			}
		} catch (Exception ex) {
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Invalid Mapping", "Error validation when pressing Next: " + ex);
			return false;
		}
		return validatedNextPressed;
	}

}
