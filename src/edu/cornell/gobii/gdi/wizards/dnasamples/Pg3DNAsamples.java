package edu.cornell.gobii.gdi.wizards.dnasamples;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import edu.cornell.gobii.gdi.utils.Utils;

import org.eclipse.swt.widgets.Button;

public class Pg3DNAsamples extends WizardPage {
	private String config, source;
	private DTOsamples dto;
	private Table tbFieldHeaders;
	private Table tbDNAsample;
	private Table tbDNArun;
	private Table tbDNAsampleProp;
	private Table tbDNArunProp;
	private String dnaRunXMLPath;

	/**
	 * Create the wizard.
	 */
	public Pg3DNAsamples(String config, DTOsamples dto, String source) {
		super("wizardPage");
		setTitle("Wizard :: DNA sample Information");
		setDescription("");
		this.config = config;
		this.source = source;
		this.dto = dto;
		
		dnaRunXMLPath = config+"/xml/Run.xml";
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
		SashForm container = new SashForm(parent, SWT.HORIZONTAL);
        int[] weights = {2,5,5};
        
		setControl(container);
		container.setLayout(new GridLayout(3, false));

		Group group = new Group(container, SWT.NONE);
		GridData gd_group = new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1);
		gd_group.heightHint = 350;
		gd_group.widthHint = 336;
		group.setLayoutData(gd_group);
		group.setText("Data File");
		group.setLayout(new GridLayout(1, false));

		tbFieldHeaders = new Table(group, SWT.BORDER | SWT.FULL_SELECTION);
		tbFieldHeaders.setLinesVisible(true);
		tbFieldHeaders.setHeaderVisible(true);
		tbFieldHeaders.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		TableColumn tableColumn = new TableColumn(tbFieldHeaders, SWT.NONE);
		tableColumn.setWidth(200);
		tableColumn.setText("Field Headers");

		//		tbFieldHeader.addListener(SWT.Activate, new Listener() {
		//			
		//			@Override
		//			public void handleEvent(Event arg0) {
		//				tbFieldHeader.clearAll();
		//				tbFieldHeader.removeAll();
		//				for(int i=0; i<dto.getHeader().size(); i++){
		//					TableItem item = new TableItem(tbFieldHeader, SWT.NONE);
		//					item.setText(0, dto.getHeader().get(i));
		//				}
		//			}
		//		});
		Utils.setDndColumnSource(tbFieldHeaders);

		SashForm composite = new SashForm(container, SWT.VERTICAL);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		tbDNAsample = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_tbDNAsample = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tbDNAsample.minimumWidth = 100;
		gd_tbDNAsample.minimumHeight = 150;
		tbDNAsample.setLayoutData(gd_tbDNAsample);
		tbDNAsample.setLinesVisible(true);
		tbDNAsample.setHeaderVisible(true);

		TableColumn tblclmnIndex = new TableColumn(tbDNAsample, SWT.NONE);
		tblclmnIndex.setWidth(100);

		TableColumn tblclmnDnasampleInformation = new TableColumn(tbDNAsample, SWT.NONE);
		tblclmnDnasampleInformation.setWidth(250);
		tblclmnDnasampleInformation.setText("DNAsample Information");

		TableColumn tableColumn_2 = new TableColumn(tbDNAsample, SWT.NONE);
		tableColumn_2.setWidth(200);
		tableColumn_2.setText("Header");

		TableColumn tableColumn_3 = new TableColumn(tbDNAsample, SWT.NONE);
		tableColumn_3.setWidth(100);
		tableColumn_3.setText("From");

		TableColumn tableColumn_4 = new TableColumn(tbDNAsample, SWT.NONE);
		tableColumn_4.setWidth(100);
		tableColumn_4.setText("To");

		Utils.unmarshalColumns(tbDNAsample, config+"/xml/Sample.xml", dto.getSampleFields(), dto.getSubSampleFields() );
		Utils.setDndColumnTarget(tbFieldHeaders, tbDNAsample, dto.getSampleFields(), dto.getSubSampleFields());
		Utils.setTableMouseLister(tbDNAsample, dto.getSampleFields());

		tbDNAsampleProp = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_tbDNAsampleProp = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tbDNAsampleProp.minimumWidth = 100;
		gd_tbDNAsampleProp.minimumHeight = 150;
		tbDNAsampleProp.setLayoutData(gd_tbDNAsampleProp);
		tbDNAsampleProp.setSize(15, 170);
		tbDNAsampleProp.setLinesVisible(true);
		tbDNAsampleProp.setHeaderVisible(true);

		TableColumn tblclmnIndex_1 = new TableColumn(tbDNAsampleProp, SWT.NONE);
		tblclmnIndex_1.setWidth(100);

		TableColumn tableColumn_9 = new TableColumn(tbDNAsampleProp, SWT.NONE);
		tableColumn_9.setWidth(250);
		tableColumn_9.setText("Property");

		TableColumn tableColumn_10 = new TableColumn(tbDNAsampleProp, SWT.NONE);
		tableColumn_10.setWidth(200);
		tableColumn_10.setText("Value");
		Utils.loadTableProps(tbDNAsampleProp, "dnasample_prop", dto.getSamplePropFields());
		Utils.setDndColumnTarget(tbFieldHeaders, tbDNAsampleProp, dto.getSamplePropFields(), null);
//		Utils.setTableMouseLister(tbDNAsampleProp, dto.getSamplePropFields());

		SashForm composite_1  = new SashForm(container, SWT.VERTICAL);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite_1.setLayout(new GridLayout(1, false));

		tbDNArun = new Table(composite_1, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_tbDNArun = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tbDNArun.minimumWidth = 100;
		gd_tbDNArun.minimumHeight = 100;
		tbDNArun.setLayoutData(gd_tbDNArun);
		tbDNArun.setLinesVisible(true);
		tbDNArun.setHeaderVisible(true);

		TableColumn tblclmnIndex_2 = new TableColumn(tbDNArun, SWT.NONE);
		tblclmnIndex_2.setWidth(100);

		TableColumn tblclmnDnarunInformation = new TableColumn(tbDNArun, SWT.NONE);
		tblclmnDnarunInformation.setWidth(250);
		tblclmnDnarunInformation.setText("DNArun / DS_DNArun Information");

		TableColumn tableColumn_6 = new TableColumn(tbDNArun, SWT.NONE);
		tableColumn_6.setWidth(200);
		tableColumn_6.setText("Header");

		TableColumn tableColumn_7 = new TableColumn(tbDNArun, SWT.NONE);
		tableColumn_7.setWidth(100);
		tableColumn_7.setText("From");

		TableColumn tableColumn_8 = new TableColumn(tbDNArun, SWT.NONE);
		tableColumn_8.setWidth(100);
		tableColumn_8.setText("To");

		Utils.unmarshalColumns(tbDNArun, dnaRunXMLPath, dto.getRunFields(), dto.getSubRunFields());
		Utils.setDndColumnTarget(tbFieldHeaders, tbDNArun, dto.getRunFields(), dto.getSubRunFields());
		Utils.setTableMouseLister(tbDNArun, dto.getRunFields());

		tbDNArunProp = new Table(composite_1, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_tbDNArunProp = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tbDNArunProp.minimumWidth = 100;
		gd_tbDNArunProp.minimumHeight = 100;
		tbDNArunProp.setLayoutData(gd_tbDNArunProp);
		tbDNArunProp.setLinesVisible(true);
		tbDNArunProp.setHeaderVisible(true);

		TableColumn tblclmnIndex_3 = new TableColumn(tbDNArunProp, SWT.NONE);
		tblclmnIndex_3.setWidth(100);

		TableColumn tableColumn_11 = new TableColumn(tbDNArunProp, SWT.NONE);
		tableColumn_11.setWidth(250);
		tableColumn_11.setText("Property");

		TableColumn tableColumn_12 = new TableColumn(tbDNArunProp, SWT.NONE);
		tableColumn_12.setWidth(200);
		tableColumn_12.setText("Value");
		Utils.loadTableProps(tbDNArunProp, "dnarun_prop", dto.getRunPropFields());
		Utils.setDndColumnTarget(tbFieldHeaders, tbDNArunProp, dto.getRunPropFields(), null);
//		Utils.setTableMouseLister(tbDNArunProp, dto.getRunPropFields());

		TableColumn tblclmnPreview = new TableColumn(tbFieldHeaders, SWT.NONE);
		tblclmnPreview.setWidth(250);
		tblclmnPreview.setText("Preview");

        container.setWeights(weights);
		container.addListener(SWT.Show, new Listener(){

			@Override
			public void handleEvent(Event arg0) {
				tbFieldHeaders.clearAll();
				tbFieldHeaders.removeAll();
				for(int i=0; i<dto.getHeader().size(); i++){
					TableItem item = new TableItem(tbFieldHeaders, SWT.NONE);
					item.setText(dto.getHeader().get(i));
					//					item.setText(0, dto.getHeader().get(i));
				}

				tbDNAsample.setEnabled(dto.getProjectID() != null);
				tbDNAsampleProp.setEnabled(dto.getProjectID() != null);

				//enable/disable DS_DANA run table for DS Wizard
				if(dto.isDataset()){

					tbDNArun.setEnabled(dto.getDatasetID()!= null);
					tbDNArunProp.setEnabled(dto.getDatasetID()!= null);

				}else{
					tbDNArun.setEnabled(dto.getExperimentID() != null);
					tbDNArunProp.setEnabled(dto.getExperimentID() != null);
				}
			}

		});
	}

	/** @override */
	public org.eclipse.jface.wizard.IWizardPage getNextPage() {
		//kind of hack to detect without overriding WizardDialog#nextPressed()
		boolean isNextPressed = "nextPressed".equalsIgnoreCase(Thread.currentThread().getStackTrace()[2].getMethodName());
		if (isNextPressed) {
			boolean validatedNextPress = this.nextPressed();
			if (!validatedNextPress) {
				return this;
			}
		}
		return super.getNextPage();
	}

	/**
	 * @see WizardDialog#nextPressed()
	 * @see WizardPage#getNextPage()
	 */
	protected boolean nextPressed() {
		boolean validatedNextPressed = true;
		boolean validDnasample = true;
		boolean validDnasampleProp = true;
		boolean validDnarun = true;
		boolean validDnarunProp = true;
		String message = "";
		try {
			/** 
			 * validate dnasample table
			 * required fields:
			 *           name
			 *           external_code
			 *           num
			 */
			if(dto.getSampleFields().size() > 0){

				if(!dto.getSampleFields().containsKey("name")){
					validDnasample = false;
					message += "DNAsample 'name' field is required!\n";
				}else{
					dto.setSampleFieldsName((String) tbDNAsample.getData("name"));	
				}

				if(!dto.getSampleFields().containsKey("external_code")){
					validDnasample = false;
					message += "DNAsample 'external_code' field is required!\n";
				}else{
					dto.setSampleFieldsExternalCode((String) tbDNAsample.getData("external_code"));
				}


				if(!dto.getSampleFields().containsKey("num")){
					validDnasample = false;
					message += "DNAsample 'num' field is required!\n";
				}

				if(!dto.getSampleFields().containsKey("uuid")){
					validDnasample = false;
					message += "DNAsample 'uuid' field is required!\n";
				}

				if(dto.getGermplasmFields().containsKey("external_code") && dto.getSampleFields().containsKey("external_code")){

					if(!(dto.getGermplasmFieldsExternalCode().equalsIgnoreCase(dto.getSampleFieldsExternalCode()))){

						validDnasample = false;
						message += "DNASample Information 'external_code' do not match with the 'external_code' field under the Germplasm Information from the previous page.\n";

					}
				}
			}
			if(dto.getSamplePropFields().size() > 0){
				if(dto.getSampleFields().size() > 0)
					validDnasampleProp = true;
				else{
					validDnasampleProp = false;
					message += "DNAsample table must be filled out!\n";
				}
			}

			/** 
			 * validate dnarun table
			 * required fields:
			 *           name
			 */
			
			if((source.equalsIgnoreCase("samples") && dto.getExperimentID() != null) || (source.equalsIgnoreCase("dataset") && dto.getDatasetID()!=null)){
				if(!dto.getRunFields().containsKey("name")){
					validDnarun = false;
					message += "DNArun 'name' field is required!\n";
				}
				
				if(dto.getSampleFields().size() > 0 || (dto.getRunFields().containsKey("name") && !source.equalsIgnoreCase("dataset"))) {
    				if(!dto.getRunFields().containsKey("dnasample_name")){
    					validDnarun = false;
    					message += "DNArun 'dnasample_name' field is required!\n";
    				}
    				if(!dto.getRunFields().containsKey("num")){
    					validDnarun = false;
    					message += "DNArun 'num' field is required!\n";
    				}
				}
			}
			if(dto.getSampleFields().size() > 0){
				if(dto.getSampleFields().containsKey("name") && dto.getRunFields().containsKey("dnasample_name")){

					String tbDNArunName = (String) tbDNArun.getData("dnasample_name");
					String tbDNAsampleName = (String) tbDNAsample.getData("name");

					if(!tbDNArunName.equals(tbDNAsampleName)){
						validDnarun = false;

						message += "DNASample Information 'name' do not match with the 'dnasample_name' field under the DNArun Information.\n";

					}
				}
				if(dto.getSampleFields().containsKey("num") && dto.getRunFields().containsKey("num")){

					String tbDNArunNum = (String) tbDNArun.getData("num");
					String tbDNAsampleNum = (String) tbDNAsample.getData("num");

					if(!tbDNArunNum.equals(tbDNAsampleNum)){

						validDnarun = false;

						message += "DNASample Information 'num' do not match with the 'num' field under the DNArun Information.\n";

					}
				}
			}
			if(dto.getRunPropFields().size() > 0){
				if(dto.getRunFields().size() > 0)
					validDnarunProp = true;
				else{
					validDnarunProp = false;
					message += "DNArun table must be filled out!\n";
				}
			}

			if(!validDnasample || !validDnasampleProp || !validDnarun || !validDnarunProp){
				MessageDialog.openError(Display.getCurrent().getActiveShell(), "Invalid Mapping", message);
				return false;
			}
		} catch (Exception ex) {
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Invalid Mapping", "Error validation when pressing Next: " + ex);
			return false;
		}
		return validatedNextPressed;
	}



}
