package edu.cornell.gobii.gdi.wizards.dnasamples;

import java.io.File;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.gobiiproject.gobiiapimodel.payload.PayloadEnvelope;
import org.gobiiproject.gobiiclient.core.gobii.GobiiClientContext;
import org.gobiiproject.gobiiclient.core.gobii.GobiiEnvelopeRestResource;
import org.gobiiproject.gobiimodel.config.RestResourceId;
import org.gobiiproject.gobiimodel.cvnames.JobPayloadType;
import org.gobiiproject.gobiimodel.dto.instructions.loader.*;
import org.gobiiproject.gobiimodel.types.GobiiFileProcessDir;
import org.gobiiproject.gobiimodel.types.GobiiProcessType;
import org.gobiiproject.gobiimodel.utils.InstructionFileValidator;

import edu.cornell.gobii.gdi.main.App;
import edu.cornell.gobii.gdi.main.Main2;
import edu.cornell.gobii.gdi.services.Controller;
import edu.cornell.gobii.gdi.utils.Utils;
import edu.cornell.gobii.gdi.utils.WizardUtils;
import edu.cornell.gobii.gdi.utils.WizardUtils.TemplateCode;
import edu.cornell.gobii.gdi.wizards.WizardFinishPage;

public class WizardDNAsamples extends Wizard {

	private static Logger log = Logger.getLogger(WizardDNAsamples.class.getName());
	private String config;
	private DTOsamples dto = new DTOsamples();
	private int piID;
	private int projectID;
	private int experimentID;

	public WizardDNAsamples(String config, int piID, int projectID, int experimentID) {

		setWindowTitle("DNA Sample Data Loading Wizard");
		this.config = config;
		this.piID = piID;
		this.projectID = projectID;
		this.experimentID = experimentID;
	}

	@Override
	public void addPages() {
	    Pg1DNAsamples firstPage = new Pg1DNAsamples(config, dto, piID, projectID, experimentID);
		addPage(firstPage);
		addPage(new Pg2DNAsamples(config, dto));
		addPage(new Pg3DNAsamples(config, dto, "samples"));
		addPage(new WizardFinishPage(config, "Wizard :: DNA sample Information", "Sample", dto, null, null, firstPage));
	}

	@Override
	public boolean performFinish() {
		try {

			String folder = new File(dto.getPreviewDTO().getDirectoryName()).getName();
			if (folder != null || !folder.isEmpty()) {
				WizardUtils.createInstructionsForWizard(folder, dto);
			} else {
				Utils.showLog( log, "Error submitting instruction file",
						new Exception("No source file(s) specified!"));
				return false;
			}

			LoaderInstructionFilesDTO instructions = null;
			if (dto.getTemplate() != null) {
				instructions = WizardUtils.loadTemplate(dto.getTemplate());
				if (!WizardUtils.createSampleInstructionsFromTemplate(getShell(), instructions, dto, folder))
					return false;
			} else {
				
				instructions = new LoaderInstructionFilesDTO();
				WizardUtils.createSampleInstructionsFromDTO(getShell(), instructions, dto, folder, false);
			}
			instructions.setInstructionFileName(folder);
			instructions.getProcedure().getMetadata().setJobPayloadType(JobPayloadType.CV_PAYLOADTYPE_SAMPLES);
			
			if (WizardUtils.confirm("Do you want to submit Instructions?")) {

				InstructionFileValidator instructionFileValidator = new InstructionFileValidator(instructions.getProcedure());

				instructionFileValidator.processInstructionFile();

				String validationStatus = instructionFileValidator.validateSampleUpload();
				if(validationStatus != null){

					MessageDialog.openError(Display.getCurrent().getActiveShell(), "Instruction file validation failed.", validationStatus);
					return false;

				}else{

					try {
						PayloadEnvelope<LoaderInstructionFilesDTO> payloadEnvelope = new PayloadEnvelope<>(instructions,
								GobiiProcessType.CREATE);
						GobiiEnvelopeRestResource<LoaderInstructionFilesDTO,LoaderInstructionFilesDTO> restResource = new GobiiEnvelopeRestResource<>(
								GobiiClientContext.getInstance(null, false).getUriFactory().resourceColl(RestResourceId.GOBII_FILE_LOAD_INSTRUCTIONS));
						PayloadEnvelope<LoaderInstructionFilesDTO> loaderInstructionFileDTOResponseEnvelope = restResource
								.post(LoaderInstructionFilesDTO.class, payloadEnvelope);
						if (!Controller.getDTOResponse(this.getShell(),
								loaderInstructionFileDTOResponseEnvelope.getHeader(), null, true)) {
							return false;
						}

					} catch (Exception err) {
						Utils.showLog( log, "Error submitting instruction file", err);
						return false;
					}
				}
			}
			WizardUtils.saveTemplate(getShell(), TemplateCode.DNA, instructions);
		} catch (Exception err) {
			Utils.showLog( log, "Error submitting instruction file", err);
			return false;
		}
		return true;
	}


}
