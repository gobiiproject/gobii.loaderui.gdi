package edu.cornell.gobii.gdi.wizards.datasets;

import java.io.File;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.gobiiproject.gobiiapimodel.payload.PayloadEnvelope;
import org.gobiiproject.gobiiapimodel.restresources.gobii.GobiiUriFactory;
import org.gobiiproject.gobiiclient.core.gobii.GobiiClientContext;
import org.gobiiproject.gobiiclient.core.gobii.GobiiEnvelopeRestResource;
import org.gobiiproject.gobiimodel.config.RestResourceId;
import org.gobiiproject.gobiimodel.cvnames.JobPayloadType;
import org.gobiiproject.gobiimodel.dto.instructions.loader.GobiiFileColumn;
import org.gobiiproject.gobiimodel.dto.instructions.loader.GobiiLoaderInstruction;
import org.gobiiproject.gobiimodel.dto.instructions.loader.*;
import org.gobiiproject.gobiimodel.types.DataSetType;
import org.gobiiproject.gobiimodel.types.GobiiColumnType;
import org.gobiiproject.gobiimodel.types.GobiiProcessType;
import org.gobiiproject.gobiimodel.utils.InstructionFileValidator;

import edu.cornell.gobii.gdi.main.App;
import edu.cornell.gobii.gdi.services.Controller;
import edu.cornell.gobii.gdi.utils.Utils;
import edu.cornell.gobii.gdi.utils.WizardUtils;
import edu.cornell.gobii.gdi.utils.WizardUtils.TemplateCode;
import edu.cornell.gobii.gdi.wizards.WizardFinishPage;
import edu.cornell.gobii.gdi.wizards.dnasamples.Pg2DNAsamples;
import edu.cornell.gobii.gdi.wizards.dnasamples.Pg3DNAsamples;
import edu.cornell.gobii.gdi.wizards.markers.Pg2Markers;
import edu.cornell.gobii.gdi.wizards.markers.Pg3Markers;

public class WizardDataset extends Wizard {
	private static Logger log = Logger.getLogger(WizardDataset.class.getName());
	private String config;
	private DTOdataset dto = new DTOdataset();
	private int piID;
	private int projectID;
	private int experimentID;
	private int datasetID;

	public WizardDataset(String config, int piID, int projectID, int experimentID, int datasetID) {
		setWindowTitle("New Wizard");
		this.config = config;
		this.piID=piID;
		this.projectID = projectID;
		this.experimentID = experimentID;
		this.datasetID = datasetID;
	}

	@Override
	public void addPages() {
		Page1Datasets firstPage = new Page1Datasets(config, dto, piID, projectID, experimentID, datasetID);
		addPage(firstPage);
		addPage(new Pg2Markers(config, dto.getDtoMarkers()));
		addPage(new Pg3Markers(config, dto.getDtoMarkers()));
		addPage(new Pg2DNAsamples(config, dto.getDtoSamples()));
		addPage(new Pg3DNAsamples(config, dto.getDtoSamples(), "dataset"));
		addPage(new WizardFinishPage(config, "Wizard :: Dataset Information", "Dataset", null, null, dto, firstPage));
	}

	@Override
	public boolean performFinish() {
		try{
			
			String folder = new File(dto.getPreviewDTO().getDirectoryName()).getName();
			if(folder != null || !folder.isEmpty()){
				WizardUtils.createInstructionsForWizard(folder, dto);
			}else{
				Utils.showLog(log, "Error submitting instruction file", new Exception("No source file(s) specified!"));
				return false;
			}
			
			LoaderInstructionFilesDTO instructions = null;
			Utils.setMarkerAndSampleDTOfromDSDTO(dto);
			if(dto.getTemplate() != null){
				instructions = WizardUtils.loadTemplate(dto.getTemplate());
				if(!WizardUtils.createMarkerInstructionsFromTemplate(getShell(), instructions, dto.getDtoMarkers(), folder)){
					return false;
				}
				if(!WizardUtils.createSampleInstructionsFromTemplate(getShell(), instructions, dto.getDtoSamples(), folder)){
					return false;
				}
				if(!WizardUtils.createDatasetInstructionsFromTemplate(getShell(), instructions, dto, folder));
			}else{
				
				instructions = new LoaderInstructionFilesDTO();

				// set marker information
				// GSD-55: if you don't set the marker file's file here, the marker file's file is there, 
				// 		   but has null values. This is because WizardUtils.createInstructionsForWizard() 
				//         sets the values for the DTOdataset's file, but does not set those of the DTOdataset
				//         marker. Ideally, this should be done in createInstructionsForWizard(). However, 
				//         I have no idea what the implications of that kind of change would be.
				
				WizardUtils.createDatasetInstructionsFromDTO(getShell(), dto, instructions, folder);
			}
			instructions.setInstructionFileName(folder);
			instructions.getProcedure().getMetadata().setJobPayloadType(JobPayloadType.CV_PAYLOADTYPE_MATRIX);
			
			if(WizardUtils.confirm("Do you want to submit Instructions?")){
				
				InstructionFileValidator instructionFileValidator = new InstructionFileValidator(instructions.getProcedure());
				
				instructionFileValidator.processInstructionFile();
				
				String validationStatus = instructionFileValidator.validate();
				if(validationStatus != null){

					MessageDialog.openError(Display.getCurrent().getActiveShell(), "Instruction file validation failed.", validationStatus);
					return false;
				}else{
				
				try {
					PayloadEnvelope<LoaderInstructionFilesDTO> payloadEnvelope = new PayloadEnvelope<>(instructions, GobiiProcessType.CREATE);
					 String currentCropContextRoot = GobiiClientContext.getInstance(null, false).getCurrentCropContextRoot();
				        GobiiUriFactory gobiiUriFactory = new GobiiUriFactory(currentCropContextRoot);
					GobiiEnvelopeRestResource<LoaderInstructionFilesDTO,LoaderInstructionFilesDTO> restResource = new GobiiEnvelopeRestResource<>(gobiiUriFactory.resourceColl(RestResourceId.GOBII_FILE_LOAD_INSTRUCTIONS));
					PayloadEnvelope<LoaderInstructionFilesDTO> loaderInstructionFileDTOResponseEnvelope = restResource.post(LoaderInstructionFilesDTO.class,
							payloadEnvelope);
					if(!Controller.getDTOResponse(this.getShell(), loaderInstructionFileDTOResponseEnvelope.getHeader(), null, true)){
						return false;
					}
				} catch (Exception err) {
					Utils.showLog(log, "Error submitting instruction file", err);
					return false;
				}
				}
			}
			WizardUtils.saveTemplate(getShell(), TemplateCode.DSS, instructions);
		}catch(Exception err){
			System.out.println(err.getMessage());
			Utils.showLog(log, "Error submitting instruction file", err);
			return false;
		}
		return true;
	}

}
