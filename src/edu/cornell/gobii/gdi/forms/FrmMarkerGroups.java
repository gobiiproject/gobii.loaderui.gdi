package edu.cornell.gobii.gdi.forms;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.gobiiproject.gobiiapimodel.payload.PayloadEnvelope;
import org.gobiiproject.gobiiapimodel.restresources.common.RestUri;
import org.gobiiproject.gobiiclient.core.gobii.GobiiClientContext;
import org.gobiiproject.gobiiclient.core.gobii.GobiiEnvelopeRestResource;
import org.gobiiproject.gobiimodel.config.RestResourceId;
import org.gobiiproject.gobiimodel.dto.auditable.*;
import org.gobiiproject.gobiimodel.dto.children.*;
import org.gobiiproject.gobiimodel.types.GobiiProcessType;

import edu.cornell.gobii.gdi.main.App;
import edu.cornell.gobii.gdi.services.Controller;
import edu.cornell.gobii.gdi.services.IDs;
import edu.cornell.gobii.gdi.utils.FormUtils;
import edu.cornell.gobii.gdi.utils.Utils;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class FrmMarkerGroups extends AbstractFrm {
	private static Logger log = Logger.getLogger(FrmPlatforms.class.getName());

	private int platformCol = -1;
	private int markerCol= -1;
	private int  alleleCol = -1;

	private Text txtName;
	private Text txtCode;
	private Text txtGermplasmGroup;
	private Table table;
	private TableViewer viewerParameters;
	private int currentMarkerGroupId=-1;
	private Button btnMarkerGroupAdd;
	private Button btnUpdate;
	ArrayList<String> tableItemMarkerNames = new ArrayList<String>();

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public FrmMarkerGroups(Shell shell, Composite parent, int style) {
		super(shell, parent, style);
		cbList.setEnabled(false);
		selectedName = ""; 
		GridLayout gridLayout = (GridLayout) cmpForm.getLayout();
		gridLayout.numColumns = 3;

		Label lblName = new Label(cmpForm, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("*Marker Group Name:");

		txtName = new Text(cmpForm, SWT.BORDER);
		txtName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		Label lblCode = new Label(cmpForm, SWT.NONE);
		lblCode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCode.setText("Code:");

		txtCode = new Text(cmpForm, SWT.BORDER);
		txtCode.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		txtCode.setEditable(false);
		txtCode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		Label lblGermplasmGroup = new Label(cmpForm, SWT.NONE);
		lblGermplasmGroup.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblGermplasmGroup.setText("Germplasm group:");

		txtGermplasmGroup = new Text(cmpForm, SWT.BORDER);
		txtGermplasmGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		new Label(cmpForm, SWT.NONE);

		viewerParameters = new TableViewer(cmpForm, SWT.BORDER);
		table = viewerParameters.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 4));

		TableViewerColumn tableViewerColumn = new TableViewerColumn(viewerParameters, SWT.NONE);
		TableColumn tblclmnName = tableViewerColumn.getColumn();
		tblclmnName.setWidth(90);
		tblclmnName.setText("Markers");

		TableViewerColumn tableViewerColumn_2 = new TableViewerColumn(viewerParameters, SWT.NONE);
		TableColumn tblclmnDefinition = tableViewerColumn_2.getColumn();
		tblclmnDefinition.setWidth(90);
		tblclmnDefinition.setText("Platforms");

		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(viewerParameters, SWT.NONE);
		TableColumn tblclmnRank = tableViewerColumn_1.getColumn();
		tblclmnRank.setWidth(150);
		tblclmnRank.setText("Favorable alleles");
		new Label(cmpForm, SWT.NONE);
		new Label(cmpForm, SWT.NONE);

		Label label_1 = new Label(cmpForm, SWT.NONE);
		label_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_1.setText("Markers:");


		Button btnImportMarkers = new Button(cmpForm, SWT.NONE);
		btnImportMarkers.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(!txtName.getText().isEmpty()){
					FileDialog dlg = new FileDialog(shell, SWT.OPEN);

					dlg.setFilterExtensions(new String[]{"*.txt"});
					dlg.setFilterNames(new String[]{"*.txt"});
					String fn = dlg.open();

					if (fn != null && fn.endsWith(".txt")) {
						boolean fileValidationPassed = true;
						ArrayList<String>  newLines = new ArrayList<String>();
						ArrayList<String>  newMarkerPlatforms = new ArrayList<String>();

						File file = new File(fn);
						try {

							platformCol = -1;
							markerCol= -1;
							alleleCol = -1;

							Scanner sc = new Scanner(file);
							String[] firstLine = sc.nextLine().split("\t");

							/*
							 ************ UNCOMMENT FOR CODE TO ALLOW COLUMN NAMES TO BE IN DIFFERENT ORDERS JUST AS LONG AS THE REQUIRED COLUMN NAMES ARE THERE 

							int ctr = 0;
							for(String s: firstLine){

								if (s.toLowerCase().equals("platform")) platformCol=ctr;
								else if(s.toLowerCase().equals("marker_name")) markerCol=ctr;
								else if(s.toLowerCase().equals("favorable_alleles"))  alleleCol=ctr;

								ctr++;
							}
							 *****************/
							if(firstLine.length<2){ // Must have atleast 2 columns for platform and marker

								MessageDialog.openError(shell, "Error", "Platform and Marker Columns are required");
								sc.close();
								fileValidationPassed = false;
							}
							else{

								if(platformCol==-1 || markerCol == -1){ //if the required column names are not found, assume proper order.

									platformCol = 1;
									markerCol=0;
									alleleCol = 2;
								}

								tableItemMarkerNames = getTableItems();
								
								try{
								while (sc.hasNextLine()) { //read file
									String readline = sc.nextLine();
									String[] row = readline.split("\t");
									if(row.length>1){
										String platform=null;
										String marker=null;

										if(!readline.replaceAll("\t", "").isEmpty()){ // ignore empty lines with just tabs
											try {platform = row[platformCol].toLowerCase();

											//check if platformName exists
											if(platform.isEmpty()){
												MessageDialog.openError(shell, "Error: Platform is required", "Some entries do not have a platform name.");
												sc.close();
												fileValidationPassed = false;
												return;
											}
											}
											catch(Exception e2){
											}

											//check if markerName exists
											try{marker = row[markerCol].toLowerCase();
											if(marker.isEmpty()){
												MessageDialog.openError(shell, "Error: Marker is required", "Some entries do not have a Marker name.");
												sc.close();
												fileValidationPassed = false;
												return;
											}
											}
											catch(Exception e2){
											}

											//check if duplicate platform and marker combination is found within the file
											String platformMarker = platform + marker;
											if(newMarkerPlatforms.contains(platformMarker)){
												MessageDialog.openError(shell, "Error: Duplicates found ", "There were duplicate markers found in the file with the same platform name.");
												sc.close();
												fileValidationPassed = false;
												return;
											}
											else {
												String allele;
												try{
													allele = row[alleleCol].replaceAll(" ", "").replaceAll("\"", "").toLowerCase();
												}catch(ArrayIndexOutOfBoundsException obe){
													allele="";
												}
												if (!validateAlleles(allele)){
													fileValidationPassed = false;
													return;
												}
												newMarkerPlatforms.add(platformMarker);
												newLines.add(readline);
											}
										}
									}else{
										MessageDialog.openError(shell, "Error", "Some entries have empty values. Platform and Marker Columns are required");
										sc.close();
										fileValidationPassed = false;
									}
								}
								}catch (Exception ex){
									Utils.showLog(log, "File Validation error", ex);
								}
								sc.close();
								if (newLines.size()>200) MessageDialog.openError(shell, "Error", "Please upload up to 200 markers only");
								else if(fileValidationPassed){

									int replace=-1;
									for(String s: newLines){
										String[] row = s.split("\t");

										if (tableItemMarkerNames.size()>0){
											String platformMarker = row[markerCol].toLowerCase()+row[platformCol].toLowerCase();

											if(replace<0){
												MessageDialog dg = new MessageDialog(
														shell,
														"Markers already exist in the table",
														null,
														"Would you like to update the favorable alleles for this marker group?",
														MessageDialog.QUESTION_WITH_CANCEL, 
														new String[]{
																"Update", 
																"Append", "Cancel"},
														0
														);
												switch(dg.open()) {
												case 0:   //overwrite
													replace=1;

													cleanMarkerTable();
													tableItemMarkerNames  = new ArrayList<String>();

													//		The codes commented out allows update for duplicate marker found
													//		int index = getTableIndex(platformMarker);
													//		try{table.getItem(index).setText(2, row[alleleCol].replaceAll(" ", "").replaceAll("\"", ""));}catch(Exception e1){}
													break;
												case 1: //append new
													//add the new markers only to the table
													replace=2;
													break;
												default: //append new
													//add the new markers only to the table
													return;
												}
											}

											//append only when overwrite is selected or if marker platform combination is not yet in the table
											if(!tableItemMarkerNames.contains(platformMarker) || replace==1){
												TableItem item = new TableItem(table, SWT.NONE);
												try{
													item.setText(1, row[platformCol]);
													item.setText(0, row[markerCol]);
													item.setText(2, row[alleleCol].replaceAll(" ", "").replaceAll("\"", ""));
												}catch(Exception e2){}
											}
										}
										else{ // just keep adding
											TableItem item = new TableItem(table, SWT.NONE);
											try{
												item.setText(1, row[platformCol]);
												item.setText(0, row[markerCol]);
												item.setText(2, row[alleleCol].replaceAll(" ", "").replaceAll("\"", ""));
											}catch(Exception e3){}
										}

									}
								}

							}
						} 
						catch (FileNotFoundException ae) {
							Utils.showLog(log, "File not Found", ae);
						}
					}else if(fn != null){
						MessageDialog.openError(shell, "Invalid File", "Only \".txt\" files are allowed.");
					}

				}else{
					MessageBox dialog = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
					dialog.setMessage("Please select a Marker Group!");
					dialog.open();
				}
			}

			private boolean validateAlleles(String allele) {
				// TODO Auto-generated method stub
				boolean isAlleleValid = true;

				if(!allele.isEmpty()){
					if(allele.contains(" ")){ // There must be no spaces
						isAlleleValid =  false;

					} else if(allele.contains(",")){ // if there are multiple alleles that are comma separated.
						for(String s: allele.split(",")){
							if(!isAlleleValueValid(s)){
								isAlleleValid = false;
								break;
							}
						}
					}else if(!isAlleleValueValid(allele)){
						isAlleleValid = false;
					}
				}
				return isAlleleValid;
			}

			private boolean isAlleleValueValid(String alleleValue) {
				// TODO Auto-generated method stub

				boolean isAlleleValid = true;

				try{

					//check if it's an integer
					Integer alleleNum = Integer.parseInt(alleleValue);



					if(alleleValue.length()==4){ // check if it is a 4-digit number

						//if it is, it should be 0000 <= x <= 1000
						if(alleleNum<0 || alleleNum >1000){
							isAlleleValid = false;
							MessageDialog.openError(shell, "Invalid Favorable allele value found", "Invalid allele:         "+alleleValue+"\n\n4-digit alleles should range from 0000 to 1000 only ");
						}

					}else if(alleleNum<0 || alleleNum>2){ // else it should have a value of 0 or 1 or 2 only

						isAlleleValid = false;
						MessageDialog.openError(shell, "Invalid Favorable allele value found", "Invalid allele:         "+alleleValue+"\n\n1-digit alleles should have a value of 0-2 only ");

					}

				}catch(NumberFormatException e){ //if it's not an integer

					if (!alleleValue.equalsIgnoreCase("a") && !alleleValue.equalsIgnoreCase("c")  && !alleleValue.equalsIgnoreCase("g") && !alleleValue.equalsIgnoreCase("t") &&
							!alleleValue.equalsIgnoreCase("+") && !alleleValue.equalsIgnoreCase("-")){

						isAlleleValid = false;
						MessageDialog.openError(shell, "Invalid Favorable allele value found", "Invalid allele:           "+alleleValue+"\n\nValid allele values:\n\nA, C, G, T, 0, 1, 2, +, -, and 4-digit number that ranges from 0000 to 1000");

					}					
				}

				return isAlleleValid;
			}
		});
		btnImportMarkers.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		btnImportMarkers.setText("Import Markers");

		Button btnExportMarkers = new Button(cmpForm, SWT.NONE);
		btnExportMarkers.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FormUtils.exportTableAsTxt(shell, table, "Markers");
			}
		});
		btnExportMarkers.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		btnExportMarkers.setText("Export Markers");

		Label label = new Label(cmpForm, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		btnMarkerGroupAdd = new Button(cmpForm, SWT.FLAT);
		btnMarkerGroupAdd.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
		btnMarkerGroupAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				if(!validate(true)) return;

				try{
					List<MarkerGroupMarkerDTO> markers = new ArrayList<MarkerGroupMarkerDTO>();
					for(TableItem item : table.getItems()){
						MarkerGroupMarkerDTO marker = new MarkerGroupMarkerDTO();
						marker.setPlatformName(item.getText(1));
						marker.setMarkerName(item.getText(0));
						marker.setFavorableAllele(item.getText(2));
						markers.add(marker);
					}
					MarkerGroupDTO markerGroupDTORequest = new MarkerGroupDTO(); //isNew ? Header.ProcessType.CREATE : Header.ProcessType.UPDATE
					markerGroupDTORequest.setName(txtName.getText());
					markerGroupDTORequest.setCode(txtName.getText()+"_"+txtGermplasmGroup.getText());
					markerGroupDTORequest.setGermplasmGroup(txtGermplasmGroup.getText());
					markerGroupDTORequest.setMarkers(markers);
					markerGroupDTORequest.setStatusId(1);
					markerGroupDTORequest.setCreatedBy(App.INSTANCE.getUser().getUserId());
					markerGroupDTORequest.setModifiedBy(App.INSTANCE.getUser().getUserId());

					PayloadEnvelope<MarkerGroupDTO> markerGroupDTOResponseEnvelope = null;
					PayloadEnvelope<MarkerGroupDTO> payloadEnvelope = new PayloadEnvelope<>(markerGroupDTORequest, GobiiProcessType.CREATE);
					GobiiEnvelopeRestResource<MarkerGroupDTO,MarkerGroupDTO> gobiiEnvelopeRestResource = new GobiiEnvelopeRestResource<>(GobiiClientContext.getInstance(null, false)
							.getUriFactory()
							.resourceColl(RestResourceId.GOBII_MARKERGROUP));
					markerGroupDTOResponseEnvelope = gobiiEnvelopeRestResource.post(MarkerGroupDTO.class,
							payloadEnvelope);

					if(!markerGroupDTOResponseEnvelope.getHeader().getStatus().isSucceeded()){
						MessageDialog.openError(shell, "Error", " Error saving Marker Group.\n\nPlease check if the marker(s) and platform(s) you are trying to save is already in the database.");
					}
					else if(Controller.getDTOResponse(shell, markerGroupDTOResponseEnvelope.getHeader(), memInfo, true)){
						currentMarkerGroupId = markerGroupDTOResponseEnvelope.getPayload().getData().get(0).getMarkerGroupId();
						populateMarkerGroupList();
						FormUtils.selectRowById(tbList, currentMarkerGroupId);
						txtCode.setText(markerGroupDTOResponseEnvelope.getPayload().getData().get(0).getCode());
						selectedName = txtName.getText();
					}


				}catch (Exception err) {
					Utils.showLog(log, "Error saving MarkerGroup", err);
				}
			}
		});
		btnMarkerGroupAdd.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		btnMarkerGroupAdd.setText("Add New MarkerGroup");

		btnUpdate = new Button(cmpForm, SWT.NONE);
		btnUpdate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				if(!validate(false)) return;
				if(!FormUtils.updateForm(getShell(), "Marker Group", selectedName)) return;

				try{
					List<MarkerGroupMarkerDTO> markers = new ArrayList<MarkerGroupMarkerDTO>();
					for(TableItem item : table.getItems()){
						MarkerGroupMarkerDTO marker = new MarkerGroupMarkerDTO();
						marker.setPlatformName(item.getText(1));
						marker.setMarkerName(item.getText(0));
						marker.setFavorableAllele(item.getText(2));
						markers.add(marker);
					}

					MarkerGroupDTO markerGroupDTORequest = new MarkerGroupDTO(); 
					markerGroupDTORequest.setMarkerGroupId(currentMarkerGroupId);
					markerGroupDTORequest.setName(txtName.getText());
					markerGroupDTORequest.setCode(txtName.getText()+"_"+txtGermplasmGroup.getText());
					markerGroupDTORequest.setGermplasmGroup(txtGermplasmGroup.getText());
					markerGroupDTORequest.setMarkers(markers);
					markerGroupDTORequest.setStatusId(1);
					markerGroupDTORequest.setCreatedBy(App.INSTANCE.getUser().getUserId());
					markerGroupDTORequest.setModifiedBy(App.INSTANCE.getUser().getUserId());

					PayloadEnvelope<MarkerGroupDTO> markerGroupDTOResponseEnvelope = null;

					RestUri restUriMapsetForGetById = GobiiClientContext.getInstance(null, false)
							.getUriFactory()
							.resourceByUriIdParam(RestResourceId.GOBII_MARKERGROUP);
					restUriMapsetForGetById.setParamValue("id", markerGroupDTORequest.getMarkerGroupId().toString());
					GobiiEnvelopeRestResource<MarkerGroupDTO,MarkerGroupDTO> gobiiEnvelopeRestResourceForGetById = new GobiiEnvelopeRestResource<>(restUriMapsetForGetById);
					gobiiEnvelopeRestResourceForGetById.setParamValue("id", markerGroupDTORequest.getMarkerGroupId().toString());
					markerGroupDTOResponseEnvelope = gobiiEnvelopeRestResourceForGetById.put(MarkerGroupDTO.class,
							new PayloadEnvelope<>(markerGroupDTORequest, GobiiProcessType.UPDATE));

					if(!markerGroupDTOResponseEnvelope.getHeader().getStatus().isSucceeded()){
						MessageDialog.openError(shell, "Error", " Error saving Marker Group.\n\nPlease make sure that the marker(s) and platform(s) you are trying to save is already in the database.");
					}
					else if(Controller.getDTOResponse(shell, markerGroupDTOResponseEnvelope.getHeader(), memInfo, true)){
						populateMarkerGroupList();
						FormUtils.selectRowById(tbList, currentMarkerGroupId);
						txtCode.setText(markerGroupDTOResponseEnvelope.getPayload().getData().get(0).getCode());
						selectedName = txtName.getText();
					}
				}catch (Exception err) {
					Utils.showLog(log, "Error saving MarkerGroup", err);
				}
			}
		});
		btnUpdate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		btnUpdate.setText("Update MarkerGroup");

		Button btnClearFields = new Button(cmpForm, SWT.NONE);
		btnClearFields.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				cleanDetails();
			}
		});
		btnClearFields.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		btnClearFields.setText("Clear Fields");



		btnRefresh.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				cleanDetails();
				currentMarkerGroupId = -1;
				selectedName = "";
				tbList.removeAll();
				FormUtils.entrySetToTable(Controller.getMarkerGroupNames(), tbList);
			}
		});
	}

	protected int getTableIndex(String platformMarker) {
		// TODO Auto-generated method stub
		int ctr=0;
		for(String s: tableItemMarkerNames){
			if(s.equals(platformMarker)) break;
			ctr++;
		}
		return ctr;
	}

	protected ArrayList<String> getTableItems() {
		// TODO Auto-generated method stub
		tableItemMarkerNames = new ArrayList<String>();

		for(TableItem item : table.getItems()){
			String s = item.getText(markerCol).toLowerCase()+item.getText(platformCol).toLowerCase();
			tableItemMarkerNames.add(s);
		}
		return tableItemMarkerNames;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	@Override
	protected void createContent() {
		TableColumn tblclmnNewColumn = new TableColumn(tbList, SWT.NONE);
		tblclmnNewColumn.setWidth(300);
		tblclmnNewColumn.setText("Marker Groups:");

		populateMarkerGroupList();
	}

	private void populateMarkerGroupList() {
		// TODO Auto-generated method stub
		tbList.removeAll();
		FormUtils.entrySetToTable(Controller.getMarkerGroupNames(), tbList);

		tbList.addListener (SWT.Selection, new Listener() {

			public void handleEvent(Event e) {
				cleanDetails();
				if(tbList.getSelectionIndex() < 0) return;
                selectedName = tbList.getSelection()[0].getText(); //single selection
				currentMarkerGroupId = FormUtils.getTableItemIdBySelectedIndex(tbList);
				populateMarkerGroupDetails(currentMarkerGroupId); //retrieve and display projects by contact Id
			}

			protected void populateMarkerGroupDetails(int markerGroupId) {
				try{
					MarkerGroupDTO MarkerGroupDTORequest = new MarkerGroupDTO();
					MarkerGroupDTORequest.setMarkerGroupId(markerGroupId);
					try {
						RestUri restUriMapsetForGetById = GobiiClientContext.getInstance(null, false)
								.getUriFactory()
								.resourceByUriIdParam(RestResourceId.GOBII_MARKERGROUP);
						restUriMapsetForGetById.setParamValue("id", Integer.toString(markerGroupId));
						GobiiEnvelopeRestResource<MarkerGroupDTO,MarkerGroupDTO> gobiiEnvelopeRestResourceForGetById = new GobiiEnvelopeRestResource<>(restUriMapsetForGetById);
						PayloadEnvelope<MarkerGroupDTO> markerGroupDTOResponseEnvelope = gobiiEnvelopeRestResourceForGetById
								.get(MarkerGroupDTO.class);

						MarkerGroupDTO markerGroupDTOResponse = markerGroupDTOResponseEnvelope.getPayload().getData().get(0);
						selectedName = markerGroupDTOResponse.getName();
						txtName.setText(selectedName);
						txtCode.setText(markerGroupDTOResponse.getCode());
						txtGermplasmGroup.setText(markerGroupDTOResponse.getGermplasmGroup());

						populateTableFromStringList(markerGroupDTOResponse.getMarkers(), table);
					} catch (Exception err) {
						Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error retrieving MarkerGroups", err);
					}
				}catch(Exception err){
					Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error retrieving MarkerGroups", err);
				}

			}
		});
	}

	private void populateTableFromStringList(List<MarkerGroupMarkerDTO> list, Table table) {
		try{
			table.removeAll();

			TableItem item = null;
			for(MarkerGroupMarkerDTO property : list){
				item= new TableItem(table, SWT.NONE); 
				item.setText(1, property.getPlatformName()); //index zero - first column 
				item.setText(0,property.getMarkerName());
				item.setText(2, property.getFavorableAllele()); //index zero - first column 
				item.setData(property.getMarkerId());
			}
		}catch(Exception err){
			Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error retrieving CVs", err);
		}
	}
	protected void cleanDetails() {
		try{
			txtName.setText("");
			txtCode.setText("");
			txtGermplasmGroup.setText("");
			cleanMarkerTable();
		}catch(Exception err){
			Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error clearing fields", err);
		}
	}

	protected void cleanMarkerTable(){
		for(TableItem item : table.getItems()){
			item.dispose();
		}
	}

	protected boolean validate(boolean isNew){
		boolean successful = true;
		String message = null;
		if(txtName.getText().isEmpty()){
			message = "Name field is required";
			successful = false;
		}else if(table.getItemCount() == 0){
			message = "There are no markers in group, please import a list of markers";
			successful = false;
		}else if(table.getItemCount() > 200){
			message = "There are more than 200 markers in group, limit is up to 200 markers only.";
			successful = false;
		}else if(!isNew && currentMarkerGroupId<=0){
			message = "'"+txtName.getText()+"' is recognized as a new value. Please use Add instead.";
			successful = false;
		}
		else if(isNew || !txtName.getText().equalsIgnoreCase(selectedName)){
			for(int i=0; i<tbList.getItemCount(); i++){
				if(tbList.getItem(i).getText(0).equalsIgnoreCase(txtName.getText())){
					successful = false;
					message = "Marker Group name already exists, kindly change name and try saving again.";
					break;
				}
			}
		}
		if(!successful){
			MessageBox dialog = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
			dialog.setMessage(message);
			dialog.open();
		}
		return successful;
	}
}
