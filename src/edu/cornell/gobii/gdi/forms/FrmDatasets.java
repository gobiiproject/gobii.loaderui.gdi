package edu.cornell.gobii.gdi.forms;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.GridLayout;
import org.gobiiproject.gobiiapimodel.payload.PayloadEnvelope;
import org.gobiiproject.gobiiapimodel.restresources.common.RestUri;
import org.gobiiproject.gobiiclient.core.gobii.GobiiClientContext;
import org.gobiiproject.gobiiclient.core.gobii.GobiiEnvelopeRestResource;
import org.gobiiproject.gobiimodel.config.RestResourceId;
import org.gobiiproject.gobiimodel.dto.noaudit.DataSetDTO;
//import org.gobiiproject.gobiiclient.dtorequests.DtoRequestMarkers;
//import org.gobiiproject.gobiimodel.dto.container.MarkerGroupDTO;
import org.gobiiproject.gobiimodel.types.GobiiProcessType;

import edu.cornell.gobii.gdi.main.App;
import edu.cornell.gobii.gdi.services.Controller;
import edu.cornell.gobii.gdi.services.IDs;
import edu.cornell.gobii.gdi.utils.FormUtils;
import edu.cornell.gobii.gdi.utils.Utils;
import edu.cornell.gobii.gdi.utils.WizardUtils;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolTip;

public class FrmDatasets extends AbstractFrm {
	private static Logger log = Logger.getLogger(FrmDatasets.class.getName());
	private Table tbAnalysis;
	private Combo cbExperiment;
	private Combo cbAnalysis;
	private Button btnAddNew;
	private Button btnUpdate;

	private Combo cbType;
	private Label lblDatasetType;
	private Button btnClearFields;
	private Button btnDatasetWizard;
	private Text txtName;
	private Label lblName;
	private Text txtDataFile;
	private Label lblDataFile;
	private Text txtDataTable;
	private Label lblDataTable;
	private TableColumn tblclmnDatasets;
	private int currentProjectId;
	private int currentExperimentId;
	protected int currentDatasetId;
	private DataSetDTO selectedDatasetDTOFromList;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 * @wbp.parser.constructor
	 */
	public FrmDatasets(final Shell shell, Composite parent, int style, final String config) {
		super(shell, parent, style);

		currentProjectId = 0;
		currentExperimentId = 0;
		currentDatasetId = 0;


		populateCbListAndTbList();
	}

	public FrmDatasets(Shell shell, Composite parent, int style, String config, int projectId,
			int experimentId) {
		super(shell, parent, style);

		currentProjectId = projectId;
		currentExperimentId = experimentId;
		currentDatasetId = 0;


		populateCbListAndTbList();

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	@Override
	protected void createContent() {
		lblCbList.setText("Experiments:");
		cbList.setText("*Select Experiment");

		btnRefresh.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				tbList.removeAll();
				Integer id = FormUtils.getIdFromFormList(cbList);
				if (currentProjectId>0){
					FormUtils.entrySetToComboSelectId(Controller.getExperimentNamesByProjectId(currentProjectId), cbList, id);
					FormUtils.entrySetToTable(Controller.getDataSetNames(), tbList);
				}
				else if(id>0){ //if there is a selected experiment
					FormUtils.entrySetToComboSelectId(Controller.getExperimentNames(), cbList, id);
					FormUtils.entrySetToTable(Controller.getDataSetNamesByExperimentId(id), tbList);
					FormUtils.entrySetToComboSelectId(Controller.getExperimentNames(), cbExperiment, currentExperimentId);
				}
				else{
					FormUtils.entrySetToCombo(Controller.getExperimentNames(), cbList);
					cbList.setText("Select Experiment");
					FormUtils.entrySetToTable(Controller.getDataSetNames(), tbList);
					cbExperiment.removeAll();
				}

				FormUtils.entrySetToCombo(Controller.getAnalysisNamesByType("calling"), cbAnalysis);
				FormUtils.entrySetToCombo(Controller.getCVByGroup("dataset_type"), cbType);

				selectedName = null;
				currentDatasetId = 0;
				selectedName=null;
				cleanDetails(false);
				tbAnalysis.removeAll();
				FormUtils.entrySetToTable(Controller.getAnalysisNames(), tbAnalysis);

			}
		});

		cbList.addListener (SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				String selected = cbList.getText(); //single selection
				cbExperiment.setText(selected);
				cleanDetails(true);
				currentExperimentId= FormUtils.getIdFromFormList(cbList);
				currentDatasetId = 0;
				selectedName=null;
				populateDatasetListFromSelectedExperiment(currentExperimentId); //retrieve and display projects by contact Id
			}

		});


		tbList.addListener (SWT.Selection, new Listener() {

			public void handleEvent(Event e) {
                selectedName = tbList.getSelection()[0].getText(); //single selection
			    currentDatasetId = FormUtils.getIdFromTableList(tbList);
				btnDatasetWizard.setEnabled(!Controller.isDatasetLoaded(currentDatasetId));
				populateDatasetDetails(currentDatasetId); //retrieve and display projects by contact Id

			}

		});


		GridLayout gridLayout = (GridLayout) cmpForm.getLayout();
		gridLayout.numColumns = 2;

		lblName = new Label(cmpForm, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("*Dataset Name:");

		txtName = new Text(cmpForm, SWT.BORDER);
		txtName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtName.addFocusListener(new FocusListener() {
			ToolTip tip = new ToolTip(shell, SWT.BALLOON);

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(cbList.getSelectionIndex()<0){

					Point loc = cbList.toDisplay(cbList.getLocation());

					tip.setMessage("Please select an Experiment before creating or updating an entry.");
					tip.setLocation(loc.x + cbList.getSize().x , loc.y-cbList.getSize().y/2);
					tip.setVisible(true);
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				tip.setVisible(false);
			}
		});

		Label lblExperiment = new Label(cmpForm, SWT.NONE);
		lblExperiment.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblExperiment.setText("*Experiment:");

		cbExperiment = new Combo(cmpForm, SWT.NONE);
		cbExperiment.setEnabled(false);
		cbExperiment.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		lblDatasetType = new Label(cmpForm, SWT.NONE);
		lblDatasetType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDatasetType.setText("*Dataset Type:");

		cbType = new Combo(cmpForm, SWT.NONE);
		cbType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		FormUtils.entrySetToCombo(Controller.getCVByGroup("dataset_type"), cbType);

		Label lblCallAnalysis = new Label(cmpForm, SWT.NONE);
		lblCallAnalysis.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCallAnalysis.setText("*Calling Analysis:");

		cbAnalysis = new Combo(cmpForm, SWT.NONE);
		cbAnalysis.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		FormUtils.entrySetToCombo(Controller.getAnalysisNamesByType("calling"), cbAnalysis);

		lblDataFile = new Label(cmpForm, SWT.NONE);
		lblDataFile.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDataFile.setText("Data File:");

		txtDataFile = new Text(cmpForm, SWT.BORDER | SWT.READ_ONLY);
		txtDataFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		lblDataTable = new Label(cmpForm, SWT.NONE);
		lblDataTable.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDataTable.setText("Data Table:");

		txtDataTable = new Text(cmpForm, SWT.BORDER | SWT.READ_ONLY);
		txtDataTable.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));


		Label lblAnalyses = new Label(cmpForm, SWT.NONE);
		lblAnalyses.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAnalyses.setText("Analyses:");

		tbAnalysis = new Table(cmpForm, SWT.BORDER | SWT.CHECK | SWT.FULL_SELECTION);
		GridData gd_tbAnalysis = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tbAnalysis.heightHint = 105;
		tbAnalysis.setLayoutData(gd_tbAnalysis);
		tbAnalysis.setHeaderVisible(true);
		tbAnalysis.setLinesVisible(true);
		new Label(cmpForm, SWT.NONE);
		FormUtils.entrySetToTable(Controller.getAnalysisNames(), tbAnalysis);

		btnAddNew = new Button(cmpForm, SWT.NONE);
		btnAddNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if(!validate(true)) return;
					saveDataset(true);
				}catch(Exception err){
					Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error saving Dataset", err);
				}
			}
		});
		btnAddNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnAddNew.setText("Add New");
		new Label(cmpForm, SWT.NONE);

		btnUpdate = new Button(cmpForm, SWT.NONE);
		btnUpdate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if(!validate(false)) return;
					if(!FormUtils.updateForm(getShell(), "Dataset", selectedName)) return;
					saveDataset(false);
				}catch(Exception err){
					Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error saving Dataset", err);
				}
			}
		});
		btnUpdate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnUpdate.setText("Update");
		new Label(cmpForm, SWT.NONE);

		btnClearFields = new Button(cmpForm, SWT.NONE);
		btnClearFields.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectedName = null;
				//				currentDatasetId = 0;
				cleanDetails(true);
				String selected = cbList.getText(); //single selection
				cbExperiment.setText(selected);
			}
		});
		btnClearFields.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnClearFields.setText("Clear Fields");
		new Label(cmpForm, SWT.NONE);

		btnDatasetWizard = new Button(cmpForm, SWT.FLAT);
		btnDatasetWizard.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			    
			    String jobStatus = Controller.isDataseCurrentlyBeingLoadedTo(currentDatasetId);
    			if(!jobStatus.isEmpty()) {
    			    MessageBox dialog = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
    	            dialog.setMessage("Data is still in the process of being loaded to this dataset.\n\n"
    	                    + "Please refresh this page after 10 minutes to check if this dataset can be loaded to.\n\n"
    	                    + "Current job status: "+ jobStatus);
    	            dialog.open();
    			}else {
    			    WizardUtils.CreateDatasetWizard(shell, App.INSTANCE.getConfigDir(), 0, currentProjectId, currentExperimentId, currentDatasetId);
    			}
			}
		});
		btnDatasetWizard.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
		btnDatasetWizard.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDatasetWizard.setText("Dataset Wizard");

		tblclmnDatasets = new TableColumn(tbList, SWT.NONE);
		tblclmnDatasets.setWidth(300);
		tblclmnDatasets.setText("Datasets:");

	}

	private void populateCbListAndTbList() {
		// TODO Auto-generated method stub
		try{
			// get experiments
			if(currentProjectId>0 ){ 
				if(currentExperimentId>0){
					FormUtils.entrySetToComboSelectId(Controller.getExperimentNamesByProjectId(currentProjectId), cbList, currentExperimentId);
					cbExperiment.setText(cbList.getText());
				}
				else FormUtils.entrySetToCombo(Controller.getExperimentNamesByProjectId(currentProjectId), cbList);
			}else{
				if(currentExperimentId>0){
					FormUtils.entrySetToComboSelectId(Controller.getExperimentNames(), cbList, currentExperimentId);
					cbExperiment.setText(cbList.getText());
				}
				else FormUtils.entrySetToCombo(Controller.getExperimentNames(), cbList);
			}
			// get datasets
			if(currentExperimentId>0) FormUtils.entrySetToTable(Controller.getDataSetNamesByExperimentId(currentExperimentId), tbList);
			else FormUtils.entrySetToTable(Controller.getDataSetNames(), tbList);

			currentDatasetId=0;
			selectedName=null;
		}catch(Exception err){
			Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error retrieving Projects and Experiemnts", err);
		}

	}

	public void populateDatasetListFromSelectedExperiment(Integer selectedId) {
		tbList.removeAll();
		try{
			if(cbList.getText().contains("*Select") || cbList.getText().isEmpty()){
				 FormUtils.entrySetToTable(Controller.getDataSetNames(), tbList);
			}else{
				FormUtils.entrySetToTable(Controller.getDataSetNamesByExperimentId(selectedId), tbList);
			}
		}catch(Exception err){
			Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error retrieving Datasets", err);
		}
	}

	private void cleanDetails(boolean unCheckAllAnalysisInTable) {
		try{
			txtName.setText("");
			cbType.deselectAll(); cbType.setText("");
			cbAnalysis.deselectAll(); cbAnalysis.setText("");
			txtDataFile.setText("");
			txtDataTable.setText("");

			if(unCheckAllAnalysisInTable){
				for(TableItem item : tbAnalysis.getItems()){
					item.setChecked(false);
				}
			}
		}catch(Exception err){
			Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error clearing fields", err);
		}
	}

	private boolean validate(boolean isNew){
		boolean successful = true;
		String message = null;
		if(txtName.getText().isEmpty()){
			message = "Name field is required!";
			successful = false;
		}else if(cbList.getSelectionIndex() < 0 && cbExperiment.getSelectionIndex() < 0){
			message = "Please select an Experiment!";
			successful = false;
		}else if(cbType.getSelectionIndex() < 0){
			message = "Dataset Type is a required field!";
			successful = false;
		}else if(cbAnalysis.getSelectionIndex() < 0){
			message = "Calling analysis is a required field!";
			successful = false;
		}else if(!isNew && selectedName==null){
			message = "You have not selected an existing dataset to update. Use \"Add New\" to add a new dataset.";
			successful = false;
		}else if(isNew && cbList.getSelectionIndex() < 0){
			message = "Please select an Experiment!";
			successful = false;
		}
		else{
			if(isNew || !txtName.getText().equalsIgnoreCase(selectedName)){

				for(TableItem item : tbList.getItems())
					if(item.getText().equalsIgnoreCase(txtName.getText())){
						message = "Name of this Dataset already exists for this Experiment!";
						successful = false;
						break;
					}
			}
		}
		if(!successful){
			MessageBox dialog = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
			dialog.setMessage(message);
			dialog.open();
		}
		return successful;
	}

	private void populateDatasetDetails(int datasetId) {
		cleanDetails(true);

		try {
			FormUtils.entrySetToComboSelectId(Controller.getExperimentNames(), cbExperiment, currentExperimentId);

			PayloadEnvelope<DataSetDTO> resultEnvelope = Controller.getDatasetDetailsById(currentDatasetId);

			if(Controller.getDTOResponse(shell, resultEnvelope.getHeader(), memInfo, false)){
				selectedDatasetDTOFromList = resultEnvelope.getPayload().getData().get(0);

				FormUtils.entrySetToComboSelectId(Controller.getExperimentNames(), cbExperiment, selectedDatasetDTOFromList.getExperimentId());
				selectedName = selectedDatasetDTOFromList.getDatasetName(); 
				
				if(selectedDatasetDTOFromList.getDatasetName() != null)
					txtName.setText(selectedDatasetDTOFromList.getDatasetName());
				
				for(int i=0; i<cbExperiment.getItemCount(); i++){
					Integer id = FormUtils.getComboIdfromIndex(cbExperiment, i);
					if(id.equals(selectedDatasetDTOFromList.getExperimentId())){
						cbExperiment.select(i);
						break;
					}
				}
				
				for(int i=0; i<cbType.getItemCount(); i++){
                    Integer id = FormUtils.getComboIdfromIndex(cbType, i);
					if(id.equals(selectedDatasetDTOFromList.getDatatypeId())){
						cbType.select(i);
						break;
					}
				}
				
				for(int i=0; i<cbAnalysis.getItemCount(); i++){
					Integer id = FormUtils.getComboIdfromIndex(cbAnalysis, i);
					if(id.equals(selectedDatasetDTOFromList.getCallingAnalysisId())){
						cbAnalysis.select(i);
						break;
					}
				}
				
				if(selectedDatasetDTOFromList.getDataFile() != null)
					txtDataFile.setText(selectedDatasetDTOFromList.getDataFile());
				if(selectedDatasetDTOFromList.getDataTable() != null)
					txtDataTable.setText(selectedDatasetDTOFromList.getDataTable());
				
				List<Integer> analysisIDs = selectedDatasetDTOFromList.getAnalysesIds();
				Integer index = 0;
				for(TableItem item : tbAnalysis.getItems()){
					if(analysisIDs.contains(FormUtils.getTableItemIdByIndex(item, index))){
						item.setChecked(true);
					}
					index++;
				}
			}
		} catch (Exception err) {
			Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error retrieving Dataset details", err);
		}
	}
	protected void saveDataset(boolean isNew){
		try{
			RestUri projectsCollUri = null;
			GobiiEnvelopeRestResource<DataSetDTO,DataSetDTO> restResource = null;
			DataSetDTO dataSetDTORequest =  new DataSetDTO();
			PayloadEnvelope<DataSetDTO> resultEnvelope = null;

			if(isNew){
				projectsCollUri =  GobiiClientContext.getInstance(null, false).getUriFactory().resourceColl(RestResourceId.GOBII_DATASETS);
				restResource = new GobiiEnvelopeRestResource<>(projectsCollUri);
				
				setDatasetDetails(dataSetDTORequest);
				
				//additional values related to "ADD" dataset only
				dataSetDTORequest.setExperimentId(FormUtils.getIdFromFormLists(cbList, cbExperiment));
				dataSetDTORequest.setCreatedBy(1);
				dataSetDTORequest.setCreatedDate(new Date());
				
				resultEnvelope = restResource.post(DataSetDTO.class, new PayloadEnvelope<>(dataSetDTORequest, GobiiProcessType.CREATE));

			}else{
				projectsCollUri =  GobiiClientContext.getInstance(null, false).getUriFactory().resourceByUriIdParam(RestResourceId.GOBII_DATASETS);
				restResource = new GobiiEnvelopeRestResource<>(projectsCollUri);
				
				dataSetDTORequest = selectedDatasetDTOFromList; //This has all the "background values" of the dataset previously selected. No longer starting from FRESH DTO
				dataSetDTORequest.setDataSetId(currentDatasetId);
				
				dataSetDTORequest.getAnalysesIds().clear(); // reset it so it just doesn't keep adding during update
				setDatasetDetails(dataSetDTORequest);
				
				restResource.setParamValue("id", dataSetDTORequest.getDataSetId().toString());
				resultEnvelope = restResource.put(DataSetDTO.class, new PayloadEnvelope<>(dataSetDTORequest, GobiiProcessType.UPDATE));
			}
			try{
				if(Controller.getDTOResponse(shell, resultEnvelope.getHeader(), memInfo, true)){
					currentDatasetId = resultEnvelope.getPayload().getData().get(0).getDataSetId();
					populateDatasetListFromSelectedExperiment(currentExperimentId);
					populateDatasetDetails(currentDatasetId);
					FormUtils.selectRowById(tbList,currentDatasetId);
				};
			}catch(Exception err){
				Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error saving Dataset", err);
			}
		}catch(Exception err){
			Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error saving Dataset", err);
		}
	}

	protected void setDatasetDetails(DataSetDTO datasetDTO){
		try{
		    
			datasetDTO.setDatatypeId(FormUtils.getComboIdfromSelectedIndex(cbType));
			datasetDTO.setDatasetName(txtName.getText());
			datasetDTO.setCallingAnalysisId(FormUtils.getComboIdfromSelectedIndex(cbAnalysis));
            datasetDTO.setStatusId(1);
			
            //setAnalysisIds
			Integer index = 0;
			for(TableItem item : tbAnalysis.getItems()){
				if(item.getChecked()){
					datasetDTO.getAnalysesIds().add(FormUtils.getTableItemIdByIndex(item, index));
				}
	            index++;
	        }
			
		}catch(Exception err){
			Utils.showErrorInfoInFormBox(shell, memInfo, log, "Error saving Dataset", err);
		}
	}
}
