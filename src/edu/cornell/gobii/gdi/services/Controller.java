package edu.cornell.gobii.gdi.services;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.gobiiproject.gobiiapimodel.payload.Header;
import org.gobiiproject.gobiiapimodel.payload.HeaderStatusMessage;
import org.gobiiproject.gobiiapimodel.payload.PayloadEnvelope;
import org.gobiiproject.gobiiapimodel.restresources.common.RestUri;
import org.gobiiproject.gobiiclient.core.gobii.GobiiClientContext;
import org.gobiiproject.gobiiclient.core.gobii.GobiiEnvelopeRestResource;
import org.gobiiproject.gobiimodel.config.RestResourceId;
import org.gobiiproject.gobiimodel.config.ServerConfig;
import org.gobiiproject.gobiimodel.config.ServerConfigItem;
import org.gobiiproject.gobiimodel.cvnames.JobProgressStatusType;
import org.gobiiproject.gobiimodel.cvnames.JobType;
import org.gobiiproject.gobiimodel.dto.noaudit.*;
import org.gobiiproject.gobiimodel.dto.auditable.*;
import org.gobiiproject.gobiimodel.dto.children.*;
import org.gobiiproject.gobiimodel.dto.system.ConfigSettingsDTO;
import org.gobiiproject.gobiimodel.types.GobiiEntityNameType;
import org.gobiiproject.gobiimodel.types.GobiiFilterType;
import org.gobiiproject.gobiimodel.types.GobiiProcessType;
import org.gobiiproject.gobiimodel.types.ServerCapabilityType;
import org.gobiiproject.gobiimodel.types.SystemUserDetail;
import org.gobiiproject.gobiimodel.types.SystemUsers;
import org.gobiiproject.gobiimodel.utils.LineUtils;

import edu.cornell.gobii.gdi.forms.FrmProtocol;
import edu.cornell.gobii.gdi.main.App;
import edu.cornell.gobii.gdi.main.Main2;
//import edu.cornell.gobii.gdi.main.PasswordDialog;
import edu.cornell.gobii.gdi.utils.Utils;

public class Controller {
    private static Logger log = Logger.getLogger(Controller.class.getName());
    public static String gobiiVersion = null;
    private static PayloadEnvelope<ContactDTO> resultEnvelope;
    private static Boolean isKDActive = false;

    private static List<NameIdDTO> testNameRetrieval(GobiiEntityNameType gobiiEntityNameType,
            GobiiFilterType gobiiFilterType,
            String filterValue) throws Exception {
        List<NameIdDTO> returnVal = null;

        RestUri namesUri =  GobiiClientContext.getInstance(null, false).getUriFactory().nameIdListByQueryParams();
        GobiiEnvelopeRestResource<NameIdDTO,NameIdDTO> restResource = new GobiiEnvelopeRestResource<>(namesUri);

        namesUri.setParamValue("entity", gobiiEntityNameType.toString().toLowerCase());

        if (GobiiFilterType.NONE != gobiiFilterType) {
            namesUri.setParamValue("filterType", StringUtils.capitalize(gobiiFilterType.toString().toUpperCase()));
            namesUri.setParamValue("filterValue", filterValue);
        }


        PayloadEnvelope<NameIdDTO> resultEnvelope = restResource
                .get(NameIdDTO.class);

        String assertionErrorStem = "Error testing name-id retrieval of entity "
                + gobiiEntityNameType.toString();

        if (GobiiFilterType.NONE != gobiiFilterType) {

            assertionErrorStem += " with filter type "
                    + gobiiFilterType.toString()
                    + " and filter value "
                    + filterValue;
        }

        assertionErrorStem += ": ";

        if(Controller.getDTOResponse(Display.getCurrent().getActiveShell(), resultEnvelope.getHeader(), null, false)){
            returnVal = resultEnvelope.getPayload().getData();
        }

        return returnVal;
    }

    public static PayloadEnvelope<ProtocolDTO> getProtocolDetailsByExperimentId(Integer experimentId) throws Exception{
        RestUri restUriProtocolsForGetDetailsByExperimentId = GobiiClientContext.getInstance(null, false)
                .getUriFactory()
                .resourceColl(RestResourceId.GOBII_EXPERIMENTS)
                .addUriParam("experimentId")
                .setParamValue("experimentId", Integer.toString(experimentId))
                .appendSegment(RestResourceId.GOBII_PROTOCOL);

        GobiiEnvelopeRestResource<ProtocolDTO,ProtocolDTO> gobiiEnvelopeRestResource = new GobiiEnvelopeRestResource<>(restUriProtocolsForGetDetailsByExperimentId);
        PayloadEnvelope<ProtocolDTO> resultEnvelope = gobiiEnvelopeRestResource
                .get(ProtocolDTO.class);

        return resultEnvelope;
    }

    public static List<NameIdDTO> getAnalysisNames() {
        List<NameIdDTO> returnVal = null;
        try {
            returnVal = testNameRetrieval(GobiiEntityNameType.ANALYSIS, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block

            Utils.showLog( log, "Error getting list of analysis names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getAnalysisTypes() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =   testNameRetrieval(GobiiEntityNameType.CV, GobiiFilterType.NAMES_BY_TYPE_NAME, "analysis_type");
        } catch (Exception err) {
            // TODO Auto-generated catch block

            Utils.showLog( log, "Error getting list of analysis types", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getReferenceNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.REFERENCE, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block

            Utils.showLog( log, "Error getting list of Reference names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getContactNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.CONTACT, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Contact names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getPIContactNames() {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.CONTACT, GobiiFilterType.NAMES_BY_TYPE_NAME, "PI");
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of PI Contact names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getProjectNamesByContactId(Integer selectedContactId) {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.PROJECT, GobiiFilterType.NAMES_BY_TYPEID, Integer.toString(selectedContactId));
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting Project names by contact id.", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getManifestNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.MANIFEST, GobiiFilterType.NONE,null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Manifest names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getPlatformNames() {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.PLATFORM, GobiiFilterType.NONE,null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Platform names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getProtocolNames() {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.PROTOCOL, GobiiFilterType.NONE,null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Protocol names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getVendorProtocolNames(){
        List<NameIdDTO> returnVal = null;
        try{
            returnVal = testNameRetrieval(GobiiEntityNameType.VENDOR_PROTOCOL, GobiiFilterType.NONE, null);
        }catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Vendor-Protocol names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getProtocolNamesByPlatformId(Integer selectedId) {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.PROTOCOL, GobiiFilterType.NAMES_BY_TYPEID, Integer.toString(selectedId));
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Protocol names by Platform id", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getVendorProtocolNamesByProtocolId(Integer selectedId) {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.VENDOR_PROTOCOL, GobiiFilterType.NAMES_BY_TYPEID, Integer.toString(selectedId));
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Vendor-Protocol names by Protocol id.", err);
        }
        return returnVal;
    }



    public static List<NameIdDTO> getExperimentNamesByProjectId(Integer selectedId) {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.EXPERIMENT, GobiiFilterType.NAMES_BY_TYPEID, Integer.toString(selectedId));
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Experiment names by Project id", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getDataSetNamesByExperimentId(Integer selectedId) {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =    testNameRetrieval(GobiiEntityNameType.DATASET, GobiiFilterType.NAMES_BY_TYPEID, Integer.toString(selectedId));
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Dataset names by Experiment id.", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getMapNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.MAPSET, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Map names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getMapTypes() {
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.CV, GobiiFilterType.NAMES_BY_TYPE_NAME, "mapset_type");
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Map types", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getMapNamesByTypeId(int mapTypeId) {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.MAPSET, GobiiFilterType.NAMES_BY_TYPEID, Integer.toString(mapTypeId));
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Map names by type id.", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getMarkerGroupNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.MARKER_GROUP, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Marker names", err);
        }
        return returnVal;
    }

    public static Set<Entry<String, List<TableColDisplay>>> getTableDisplayNames() {
         

        DisplayDTO displayDTORequest = new DisplayDTO();
        displayDTORequest.getTableNamesWithColDisplay();
        displayDTORequest.setIncludeDetailsList(true);

        DisplayDTO displayDTOResponse = null;
        try {
            RestUri restUriDisplay = GobiiClientContext.getInstance(null,false)
                    .getUriFactory()
                    .resourceColl(RestResourceId.GOBII_DISPLAY);
            GobiiEnvelopeRestResource<DisplayDTO,DisplayDTO> gobiiEnvelopeRestResource = new GobiiEnvelopeRestResource<>(restUriDisplay);
            PayloadEnvelope<DisplayDTO> resultEnvelope = gobiiEnvelopeRestResource.get(DisplayDTO.class);
            displayDTOResponse = resultEnvelope.getPayload().getData().get(0);

            return displayDTOResponse.getTableNamesWithColDisplay().entrySet();
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Table Display names", err);
        }
        return null;
    }

    public static List<NameIdDTO> getAnalysisNamesByTypeId(int analysisTypeId) {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.ANALYSIS, GobiiFilterType.NAMES_BY_TYPEID, Integer.toString(analysisTypeId));
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Analysis names by type id.", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getAnalysisNamesByType(String string) {
         
        List<NameIdDTO> types = getAnalysisTypes();
        int typeId = 0;
        for (NameIdDTO entry : types) {
            if (entry.getName().equals(string)) {
                typeId = entry.getId();
                break;
            }
        }

        return getAnalysisNamesByTypeId(typeId);
    }

    public static Integer getCuratorRoleId() {
         
        Integer returnVal = 0;
        try {
            List<NameIdDTO> roleNameIDs =  testNameRetrieval(GobiiEntityNameType.ROLE,GobiiFilterType.NONE,null);

            for (NameIdDTO entry : roleNameIDs){ //add contact on list
                String input = entry.getName();
                if(input.equalsIgnoreCase("curator")) returnVal = entry.getId();
            }

        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Role names", err);
        }
        return returnVal;
    }

    public static Integer getAdminRoleId() {
         
        Integer returnVal = 0;
        try {
            List<NameIdDTO> roleNameIDs =  testNameRetrieval(GobiiEntityNameType.ROLE,GobiiFilterType.NONE,null);

            for (NameIdDTO entry : roleNameIDs){ //add contact on list
                String input = entry.getName();
                if(input.equalsIgnoreCase("admin")) returnVal = entry.getId();
            }

        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Role names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getRoleNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.ROLE,GobiiFilterType.NONE,null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Role names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getContactNamesByType(String selected) {
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.CONTACT, GobiiFilterType.NAMES_BY_TYPE_NAME, selected);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Contact names by type.", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getCVByGroup(String selectedGroup) {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal = testNameRetrieval(GobiiEntityNameType.CV, GobiiFilterType.NAMES_BY_TYPE_NAME, selectedGroup);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of CV terms by group.", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getCvGroupNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.CVGROUP, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Cv Group names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getCvNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.CV, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Cv names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getProjectNames() {
         
        // Authenticator.authenticate
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.PROJECT, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Project names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getExperimentNames() {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =   testNameRetrieval(GobiiEntityNameType.EXPERIMENT, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Experiment names", err);
        }
        return returnVal;
    }

    public static List<NameIdDTO> getDataSetNames() {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =   testNameRetrieval(GobiiEntityNameType.DATASET, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Dataset names", err);
        }
        return returnVal;
    }

    public static void showException(Shell shell, StyledText memo, Throwable throwable) {

        MessageBox dialog = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
        dialog.setText("Exception");
        dialog.setMessage("An Exception ocurred. Please refer to message box for details");
        String message = throwable.getMessage() + ": " + throwable.getStackTrace().toString();
        if (memo == null) {
            dialog.setMessage(message);
        } else {
            memo.setText(message);
        }
        dialog.open();


    }

    public static boolean getDTOResponse(Shell shell, Header header, StyledText memo, Boolean showSuccess) {
        MessageBox dialog;
        boolean headerStatusIsSuccessful=false;
        if (!header.getStatus().isSucceeded()) {
            dialog = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
            dialog.setText("Request information");
            dialog.setMessage("ERROR processing request. Please refer to the message box for details.");
            String message = "";
            for (HeaderStatusMessage currentStatusMesage : header.getStatus().getStatusMessages()) {
                message += currentStatusMesage.getMessage() + "\n";
            }
            if(message.contains("There are no files of the specified format in the directory")) message = "No file with the selected format exists";
            else if(message.contains("MESSAGE: Parameter is not allowed to be null : typeId;")) {
                message="Seems like you are trying to update a platform that does not exist in the Controlled Vocabulary.\n\n"
                        + "Please create the platform in the CV by following the steps below:\n\n"
                        + "1. Open the Controlled Vocabulary form\n"
                        + "2. Select platform_type from the CV groups\n"
                        + "3. Click the Insert a new record button\n"
                        + "4. Next to the checked box, type the name of the platform in the 'Terms' column.\n"
                        + "5. Add a brief definition\n"
                        + "6. Click Save. You can then refresh this page and update the platform."
                        + "\n\nNOTE: Make sure the spelling of the platform you created in the CV form matches the name of the platform you are trying to update here.";
            }
            else if(message.contains("MESSAGE: org.hibernate.exception.ConstraintViolationException: error executing work;")) {
               if(message.contains("deleteCv")) {
                   message = "An item cannot be deleted because it's been added as a Platform through the Platforms tab.";
               } 
            }else if(message.contains("There is no user cvgroup for the specified CV group")) {
                message = "A user-defined value is not allowed for this CV group.";
            }

            if (memo == null) {
                dialog.setMessage(message);
            } else {
                memo.setText(message);
            }
            dialog.open();
        } else if(showSuccess){
            dialog = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK);
            dialog.setText("Request information");
            dialog.setMessage("Request processed successfully!");
            headerStatusIsSuccessful = true;
            dialog.open();
        } else{
            headerStatusIsSuccessful = true;
        }
        setGobiiVersion(header.getGobiiVersion());

        return headerStatusIsSuccessful;
    }

    public static List<NameIdDTO> getPlatformNamesByTypeId(int platformTypeId) {
         
        List<NameIdDTO> returnVal = null;
        try {
            returnVal =testNameRetrieval(GobiiEntityNameType.PLATFORM, GobiiFilterType.NAMES_BY_TYPEID, Integer.toString(platformTypeId));
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Platform names by type id.", err);
        }
        return returnVal;
    }

    public static boolean getCrop(Logger log, boolean refresh, boolean isSSH, boolean showUserDialog){
        try {
            String url = App.INSTANCE.getService();

            if (refresh) {
                GobiiClientContext.resetConfiguration();
                GobiiClientContext.getInstance(url, true).getCurrentClientCropType();

                // parse URL for the context root

                URL iURL = new URL(url);

                String contextRoot = iURL.getPath();

                if('/' != contextRoot.charAt(contextRoot.length() -1)) {

                    contextRoot = contextRoot + "/";

                }

                List<String> crops = GobiiClientContext.getInstance(null, false).getCropTypeTypes();


                String crop = null;
                for (String currentCrop : crops) {

                    ServerConfigItem currentServerConfig = GobiiClientContext.getInstance(null, false).getServerConfig(currentCrop);

                    if(contextRoot.equals(currentServerConfig.getContextRoot())) {
                        // use the crop for this server config
                        crop = currentCrop;
                        break;
                    }

                }

                if(crop.isEmpty()) {

                    Utils.showErrorInfo(log, "Undefined crop for server: " +  url);

                    return false;

                }else{
                    App.INSTANCE.setCrop(crop);
                }

                if (App.INSTANCE.getService().contains("localhost") && isSSH) {
                    String hostPort = Utils.getFromTo(App.INSTANCE.getService(), "localhost:", "/");
                    Integer port = Integer.parseInt(hostPort);
                    GobiiClientContext.setSshOverride("localhost", port);
                }

            }

        } catch (Exception err) {
            // TODO Auto-generated catch block

            MessageDialog.openError(Display.getDefault().getActiveShell(), "Error in getting crop", err.getMessage().split("\n")[0]);
            return false;
        }

        return true;
    }

    public static boolean authenticate(String cropId, String uname, String pw) {
        try {
            //get URL from login form (THis has already been validated if it's in the correct format when GobiiClientContext.getCrop() was called
            String gobiiURL = App.INSTANCE.getService();
            URL url = new URL(gobiiURL);
            
            ServerConfigItem currentServerConfig = GobiiClientContext.getInstance(null, false).getServerConfig(cropId);
            currentServerConfig.setDomain(url.getHost());
            
            if (url.getPort() > 0) {
                currentServerConfig.setPort(url.getPort());
            }
            else {
                if(url.getProtocol().contains("https")) currentServerConfig.setPort(443);
                else currentServerConfig.setPort(8080);
            }
            
            boolean loginSuccessful = GobiiClientContext.getInstance(null, false).login(cropId, uname, pw);

            if(loginSuccessful){
          
                //get KDC is Active from server
                Map<ServerCapabilityType, Boolean> serverCapabilities = GobiiClientContext.getInstance(App.INSTANCE.getService(), true).getServerCapabilities();
                Boolean isActive = serverCapabilities.get(ServerCapabilityType.KDC);

                if(isActive==null){
                    isActive = false;
                }

                setIsKDActive(isActive);
                return true;
                
            } else {
                String failureMessage = GobiiClientContext.getInstance(null, false).getLoginFailure();
                if ( ! LineUtils.isNullOrEmpty(failureMessage) ) {
                    MessageDialog.openError(Display.getDefault().getActiveShell(), "Connection error", failureMessage );

                } else {
                    MessageDialog.openError(Display.getDefault().getActiveShell(), "" ,"Connection error");
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block

            Utils.showLog(log, "Error connecting to service.\n\n"+e.getMessage(), e);
            MessageDialog.openError(Display.getDefault().getActiveShell(), "Connection error", e.getMessage() );

        }

        return false;
    }


    public static boolean isNewContactEmail(String email) {
        boolean isNewEmail = true;
        try {
            RestUri restUriContact =  GobiiClientContext.getInstance(null, false).getUriFactory().contactsByQueryParams();
            restUriContact.setParamValue("email", email);
            GobiiEnvelopeRestResource<ContactDTO,ContactDTO> restResource = new GobiiEnvelopeRestResource<>(restUriContact);
            PayloadEnvelope<ContactDTO> resultEnvelope = restResource
                    .get(ContactDTO.class);

            if(Controller.getDTOResponse(Display.getCurrent().getActiveShell(), resultEnvelope.getHeader(), null, false)){
                ContactDTO contactDTO = resultEnvelope.getPayload().getData().get(0);
                if (contactDTO.getEmail() != null)
                    isNewEmail = false;
            }

        } catch (IndexOutOfBoundsException IOE) {
            // TODO Auto-generated catch block
            //nothing was returned
            isNewEmail = true;
        }catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting contact email status.", err);
        }

        return isNewEmail;
    }

    public static boolean isNewContactUsername(String username) {
        boolean isNewUsername = true;
        try {
            RestUri restUriContact =  GobiiClientContext.getInstance(null, false).getUriFactory().contactsByQueryParams();
            restUriContact.setParamValue("userName", username);
            GobiiEnvelopeRestResource<ContactDTO,ContactDTO> restResource = new GobiiEnvelopeRestResource<>(restUriContact);
            PayloadEnvelope<ContactDTO> resultEnvelope = restResource
                    .get(ContactDTO.class);

            if(Controller.getDTOResponse(Display.getCurrent().getActiveShell(), resultEnvelope.getHeader(), null, false)){
                ContactDTO contactDTO = resultEnvelope.getPayload().getData().get(0);
                if (contactDTO.getId() != null)
                    isNewUsername = false;
            }

        } catch (IndexOutOfBoundsException IOE) {
            // TODO Auto-generated catch block
            //nothing was returned
            isNewUsername = true;
        }catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting contact by username status.", err);
        }

        return isNewUsername;
    }

    public static PayloadEnvelope<ContactDTO> getContactByUsername(String userName) throws Exception {
        RestUri restUriContact = GobiiClientContext.getInstance(null, false)
                .getUriFactory()
                .contactsByQueryParams();
        restUriContact.setParamValue("userName", userName);
        GobiiEnvelopeRestResource<ContactDTO,ContactDTO> gobiiEnvelopeRestResourceForGet = new GobiiEnvelopeRestResource<>(restUriContact);
        resultEnvelope = gobiiEnvelopeRestResourceForGet
                .get(ContactDTO.class);


        return resultEnvelope;
    }

    public static List<NameIdDTO> getOrganizationNames() {
         

        List<NameIdDTO> returnVal = null;
        try {
            returnVal =  testNameRetrieval(GobiiEntityNameType.ORGANIZATION, GobiiFilterType.NONE, null);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting list of Organization names", err);
        }
        return returnVal;
    }

    public static Integer getPlatformIdByExperimentId(Integer experimentID) {
         
        ProtocolDTO protocolDTO = null;
        PayloadEnvelope<ProtocolDTO> protocolDetails;
        try {
            protocolDetails = getProtocolDetailsByExperimentId(experimentID);
            protocolDTO = protocolDetails.getPayload().getData().get(0);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting protocol details by experiment id", err);
        }


        return protocolDTO.getPlatformId();
    }

    public static PayloadEnvelope<ExperimentDTO> getExperimentDetailsById(int experimentId) {
         
        PayloadEnvelope<ExperimentDTO> resultEnvelope = null;
        RestUri experimentsUri;
        try {
            experimentsUri =  GobiiClientContext.getInstance(null, false).getUriFactory().resourceByUriIdParam(RestResourceId.GOBII_EXPERIMENTS);
            experimentsUri.setParamValue("id", Integer.toString(experimentId));
            GobiiEnvelopeRestResource<ExperimentDTO,ExperimentDTO> restResourceForExperiments = new GobiiEnvelopeRestResource<>(experimentsUri);
            resultEnvelope = restResourceForExperiments.get(ExperimentDTO.class);
        } catch (Exception err) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting experiment details by experiment id", err);
        }

        return resultEnvelope;
    }

    public static PayloadEnvelope<ProtocolDTO> getProtocolDetails(int protocolId) {
         

        PayloadEnvelope<ProtocolDTO> resultEnvelopeForGetByID = null;
        RestUri restUriProtocolForGetById;
        try {
            restUriProtocolForGetById =  GobiiClientContext.getInstance(null, false).getUriFactory()
                    .resourceByUriIdParam(RestResourceId.GOBII_PROTOCOL);

            restUriProtocolForGetById.setParamValue("id", Integer.toString(protocolId));
            GobiiEnvelopeRestResource<ProtocolDTO,ProtocolDTO> restResourceForGetById = new GobiiEnvelopeRestResource<>(restUriProtocolForGetById);

            resultEnvelopeForGetByID = restResourceForGetById
                    .get(ProtocolDTO.class);
        } catch (Exception err) {
            // TODO Auto-generated catch block

            Utils.showLog( log, "Error getting protocol details by protocol id", err);
        }

        return resultEnvelopeForGetByID;
    }

    public static PayloadEnvelope<PlatformDTO> getPlatformDetails(int platformId) {
         
        PayloadEnvelope<PlatformDTO> resultEnvelopeForGetByID = null;
        RestUri restUriPlatformForGetById;
        try {
            restUriPlatformForGetById =  GobiiClientContext.getInstance(null, false).getUriFactory()
                    .resourceByUriIdParam(RestResourceId.GOBII_PLATFORM);
            restUriPlatformForGetById.setParamValue("id", Integer.toString(platformId));
            GobiiEnvelopeRestResource<PlatformDTO,PlatformDTO> restResourceForGetById = new GobiiEnvelopeRestResource<>(restUriPlatformForGetById);

            resultEnvelopeForGetByID = restResourceForGetById
                    .get(PlatformDTO.class);
        } catch (Exception err) {
            // TODO Auto-generated catch block

            Utils.showLog( log, "Error getting platform details by platform id", err);
        }

        return resultEnvelopeForGetByID;
    }

    public static String getGobiiVersion() {
         

        return gobiiVersion;
    }

    public static void setGobiiVersion(String gobiiVersion) {
        Controller.gobiiVersion = gobiiVersion;
    }

    public static boolean checkVersionCompatibility(Boolean showSuccess) {
         
        boolean isCompatible = true; 

        //commenting this out for now, because build no. cannot be compared with how the current loader UI version is being displayed
        //		String minimumGobiiVersion = Main2.getLoaderUiVersion();
        //		String headerVersion = getGobiiVersion();
        //		if(minimumGobiiVersion.compareTo(headerVersion)>0){
        //			if(showSuccess) MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "ERROR in Loader", "The version from gobii-web is less than the minimum acceptable version for this GDI.");
        //			isCompatible = false;
        //		}

        Main2.updateVersionsOnMainWindow();
        return isCompatible;
    }

    public static PayloadEnvelope<DataSetDTO> getDatasetDetailsById(int currentDatasetId) {
         
        PayloadEnvelope<DataSetDTO> returnVal = null;
        RestUri projectsUri;
        try {
            projectsUri =  GobiiClientContext.getInstance(null, false).getUriFactory().resourceByUriIdParam(RestResourceId.GOBII_DATASETS);
            projectsUri.setParamValue("id", Integer.toString(currentDatasetId));
            GobiiEnvelopeRestResource<DataSetDTO,DataSetDTO> gobiiEnvelopeRestResource = new GobiiEnvelopeRestResource<>(projectsUri);

            returnVal = gobiiEnvelopeRestResource.get(DataSetDTO.class);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting Dataset details by  id", e);
        }

        return returnVal;
    }

    public static PayloadEnvelope<ProjectDTO> getProjectDetailsById(int projectId){
         
        PayloadEnvelope<ProjectDTO> returnVal = null;
        try {
            RestUri projectsUri =  GobiiClientContext.getInstance(null, false).getUriFactory().resourceByUriIdParam(RestResourceId.GOBII_PROJECTS);
            projectsUri.setParamValue("id", Integer.toString(projectId));
            GobiiEnvelopeRestResource<ProjectDTO,ProjectDTO> restResourceForProjects = new GobiiEnvelopeRestResource<>(projectsUri);
            returnVal = restResourceForProjects.get(ProjectDTO.class);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting Project details by  id", e);
        }

        return returnVal;
    }

    public static Boolean getIsKDActive() {
        return isKDActive;
    }

    public static void setIsKDActive(Boolean isKDActive) {
        if(isKDActive != null) Controller.isKDActive = isKDActive;
    }

    public static boolean isDatasetLoaded(int currentDatasetId) {
         
        Boolean dataLoaded = false;
        GobiiEnvelopeRestResource<JobDTO,JobDTO> restResource = null;
        try {
            restResource = new GobiiEnvelopeRestResource<>(GobiiClientContext.getInstance(null, false).getUriFactory().resourceColl(RestResourceId.GOBII_DATASETS)
                    .addUriParam("datasetId")
                    .setParamValue("datasetId", Integer.toString(currentDatasetId))
                    .appendSegment(RestResourceId.GOBII_JOB));

            PayloadEnvelope<JobDTO> jobDTOResponseEnvelope = restResource.get(JobDTO.class);

            if(!jobDTOResponseEnvelope.getPayload().getData().isEmpty()) {
                JobDTO job = jobDTOResponseEnvelope.getPayload().getData().get(0);
                if( isLoadJobStatusCompleted(job) || isExtractJob(job)){

                    dataLoaded = true;
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return dataLoaded;
    }


    private static boolean isExtractJob(JobDTO job) {
    
        //check if the Job type is EXTRACT
        return job.getType().equals(JobType.CV_JOBTYPE_EXTRACT.getCvName());
    }

    private static boolean isLoadJobStatusCompleted(JobDTO job) {
         
        //Check if the job is a data load and if the status is completed.

        return job.getStatus().equals(JobProgressStatusType.CV_PROGRESSSTATUS_COMPLETED.getCvName()) && job.getType().equals(JobType.CV_JOBTYPE_LOAD.getCvName());
    }

    public static String isDataseCurrentlyBeingLoadedTo(int currentDatasetId) {
         
        String dataLoaded = "";
        GobiiEnvelopeRestResource<JobDTO,JobDTO> restResource = null;
        try {
            restResource = new GobiiEnvelopeRestResource<>(GobiiClientContext.getInstance(null, false).getUriFactory().resourceColl(RestResourceId.GOBII_DATASETS)
                    .addUriParam("datasetId")
                    .setParamValue("datasetId", Integer.toString(currentDatasetId))
                    .appendSegment(RestResourceId.GOBII_JOB));

            PayloadEnvelope<JobDTO> jobDTOResponseEnvelope = restResource.get(JobDTO.class);

            if(!jobDTOResponseEnvelope.getPayload().getData().isEmpty()){
                JobDTO job = jobDTOResponseEnvelope.getPayload().getData().get(0);
                if( !isLoadJobStatusCompleted(job) && !isLoadJobStatusFailed(job)){

                    dataLoaded = jobDTOResponseEnvelope.getPayload().getData().get(0).getMessage();
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return dataLoaded;
    }

    private static boolean isLoadJobStatusFailed(JobDTO job) {
     
      //Check if the job is a data load and if the status is failed.
        return job.getStatus().equals(JobProgressStatusType.CV_PROGRESSSTATUS_FAILED.getCvName()) && job.getType().equals(JobType.CV_JOBTYPE_LOAD.getCvName());
    }

    public static String getPlatformNameByVendorProtocolId(int vendorProtocolId) {
         
        GobiiEnvelopeRestResource<PlatformDTO,PlatformDTO> restResource = null;
        String platfomName= null;
        try {
            restResource = new GobiiEnvelopeRestResource<>(GobiiClientContext.getInstance(null, false).getUriFactory().resourceColl(RestResourceId.GOBII_PLATFORM)
                    .appendSegment(RestResourceId.GOBII_PROTOCOL)
                    .addUriParam("vendorProtocolId")
                    .setParamValue("vendorProtocolId", Integer.toString(vendorProtocolId)));

            PayloadEnvelope<PlatformDTO> platformDTOResponseEnvelope = restResource.get(PlatformDTO.class);

            if(!platformDTOResponseEnvelope.getPayload().getData().isEmpty() && platformDTOResponseEnvelope.getPayload().getData().get(0).getPlatformId() > 0){

                platfomName = platformDTOResponseEnvelope.getPayload().getData().get(0).getPlatformName();

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Utils.showLog( log, "Error getting Platform details by Vendor-Protocol id", e);
        }


        return platfomName;
    }
}
