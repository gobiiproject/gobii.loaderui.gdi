Run GDI.jar through the command line terminal by typing

java -XstartOnFirstThread - jar GDI.jar

if that does not work type this first to make the jar file executable:

chmod +x GDI.jar

then type the run command again:

java - jar GDI.jar


Note: Make sure that you are typing those commands in the folder where your jar file is located. The config folder from the distribution package should also be in the same location as the jar file.
