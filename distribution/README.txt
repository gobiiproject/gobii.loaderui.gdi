To create the executable jar file for each distribution, the correct SWT jar for the specified OS must be the one imported in the build path.

For Mac --> swt-4.7.2-cocoa-macosx-x86_64.jar
For Linux --> swt-4.7.2-gtk-linux-x86_64.jar
For Windows --> org.eclipse.swt.win32.win32.x86_64_3.104.2.v20160212-1350.jar

All of these jars can be found in the lib folder of the project. Only one of these jars should be imported in the build path.

